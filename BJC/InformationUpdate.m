
//
//  InformationUpdate.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "InformationUpdate.h"
#import "RequestModelADV.h"
#import "MBProgressHUD.h"

#import <Google/Analytics.h>


@interface InformationUpdate ()
{
    MBProgressHUD *hudView;
    NSDictionary *objectGold, *objectOil, *objectLotto;
}
@end

@implementation InformationUpdate

- (void)viewDidLoad {
    [super viewDidLoad];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    [self reqGold :^{
        [self reqOil:^{
          [self reqLotto:^{
        
              self.dateTimeLabel.text = [NSString stringWithFormat:@"ประจำวันที่ %@ เวลา %@ น.", [objectGold objectForKey:@"date"], [objectGold objectForKey:@"time"]];
              
              NSArray *listPriceGold = [[NSArray alloc]initWithArray:(NSArray *)[objectGold objectForKey:@"tables"]];
//              NSDictionary *goldBarInfo = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceGold objectAtIndex:listPriceGold.count-2]];
//              NSDictionary *goldInfo = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceGold objectAtIndex:listPriceGold.count-1]];
              
              NSDictionary *goldBarInfo = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceGold objectAtIndex:0]];
              NSDictionary *goldInfo = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceGold objectAtIndex:1]];
              self.priceBuyGoldBarLabel.text = [NSString stringWithFormat:@"%@", [goldBarInfo objectForKey:@"2"]];
              self.priceSaleGoldBarLabel.text = [NSString stringWithFormat:@"%@", [goldBarInfo objectForKey:@"3"]];
              self.priceBuyGoldLabel.text = [NSString stringWithFormat:@"%@", [goldInfo objectForKey:@"2"]];
              self.priceSaleGoldLabel.text = [NSString stringWithFormat:@"%@", [goldInfo objectForKey:@"3"]];
            
            NSArray *listPriceOil = [[NSArray alloc]initWithArray:(NSArray *)[objectOil objectForKey:@"tables"]];
            
            NSDictionary *gasoline91 = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:1]];
            NSDictionary *gasohol91 = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:3]];
            NSDictionary *gasoline95 = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:0]];
            NSDictionary *gasoholE20 = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:4]];
              NSDictionary *gasoholE85 = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:9]];
              NSDictionary *blueDiesel = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:2]];
            NSDictionary *ngv = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPriceOil objectAtIndex:5]];
              
            self.oline91Label.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [gasoline91 objectForKey:@"price"]]];
            self.hol91Label.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [gasohol91 objectForKey:@"price"]]];
            self.hol95Label.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [gasoline95 objectForKey:@"price"]]];
            self.holE20Label.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [gasoholE20 objectForKey:@"price"]]];
              self.holE85Label.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [gasoholE85 objectForKey:@"price"]]];
              self.diesalLabel.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [blueDiesel objectForKey:@"price"]]];
            self.ngvLabel.text = [self convertFloatToString:[NSString stringWithFormat:@"%@", [ngv objectForKey:@"price"]]];
              
            NSString *dateOilString = [gasoline91 objectForKey:@"date"];
            
              if([self isEndDateIsSmallerThanCurrent:dateOilString : [gasohol91 objectForKey:@"date"]]) {
                  dateOilString = [gasohol91 objectForKey:@"date"];
              }else {
                  if([self isEndDateIsSmallerThanCurrent:dateOilString : [gasoline95 objectForKey:@"date"]]) {
                      dateOilString = [gasoline95 objectForKey:@"date"];
                  }else {
                      if([self isEndDateIsSmallerThanCurrent:dateOilString : [gasoholE20 objectForKey:@"date"]]) {
                          dateOilString = [gasoholE20 objectForKey:@"date"];
                      }else {
                          if([self isEndDateIsSmallerThanCurrent:dateOilString : [gasoholE85 objectForKey:@"date"]]) {
                              dateOilString = [gasoholE85 objectForKey:@"date"];
                          }else {
                              if([self isEndDateIsSmallerThanCurrent:dateOilString : [blueDiesel objectForKey:@"date"]]) {
                                  dateOilString = [blueDiesel objectForKey:@"date"];
                              }else {
                                  if([self isEndDateIsSmallerThanCurrent:dateOilString : [ngv objectForKey:@"date"]]) {
                                      dateOilString = [blueDiesel objectForKey:@"date"];
                                  }
                              }
                          }
                      }
                  }
              }
              
              dateOilString = [NSString stringWithFormat:@"%@/%@/%@", [dateOilString substringWithRange:NSMakeRange(0, 4)], [dateOilString substringWithRange:NSMakeRange(5, 2)], [dateOilString substringWithRange:NSMakeRange(8, 2)]];

              
            NSString *timeOilString = [NSString stringWithFormat:@"%@", [[NSString stringWithFormat:@"%@", [gasoline91 objectForKey:@"date"]] substringFromIndex:11]];
            self.dateLabel.text = [NSString stringWithFormat:@"ประจำวันที่ %@", dateOilString];
            self.timeLabel.text = [NSString stringWithFormat:@"เวลา %@ น.", timeOilString];
              
              NSString *dateLottoString = [NSString stringWithFormat:@"%@", [objectLotto objectForKey:@"date"]];
              dateLottoString = [NSString stringWithFormat:@"%@/%@/%@", [dateLottoString substringWithRange:NSMakeRange(8, 2)], [dateLottoString substringWithRange:NSMakeRange(5, 2)], [dateLottoString substringWithRange:NSMakeRange(0, 4)]];
              self.dateLottoLabel.text = [NSString stringWithFormat:@"ตรวจผลสลาก ประจำวันที่ %@", dateLottoString];
              self.numberLottoFirst.text = [NSString stringWithFormat:@"%@", [objectLotto objectForKey:@"reward1"]];
              self.numberLottoSecond.text = [NSString stringWithFormat:@"%@", [objectLotto objectForKey:@"reward2"]];
              self.numberLottoThird.text = [NSString stringWithFormat:@"%@", [objectLotto objectForKey:@"reward3"]];
              self.numberLottoThirdBack.text = [NSString stringWithFormat:@"%@", [objectLotto objectForKey:@"reward4"]];
              [hudView hide:YES];
          }];
        }];
    }];
}

- (BOOL)isEndDateIsSmallerThanCurrent:(NSString *)checkStartDate :(NSString *)checkEndDate
{
    checkStartDate = [checkStartDate substringWithRange:NSMakeRange(0, 10)];
    checkEndDate = [checkEndDate substringWithRange:NSMakeRange(0, 10)];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *dateFromString = [dateFormatter dateFromString: checkStartDate];
    NSDate* enddate = [dateFormatter dateFromString: checkEndDate];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:dateFromString];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

- (NSString *)convertFloatToString:(NSString *)value
{
    float floatResult = [value floatValue];
    NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", floatResult];
    return formattedNumber;
}


- (void)reqGold :(void (^)(void))completion {
    
    [hudView show:YES];
    RequestModelADV *service = [[RequestModelADV alloc]initGetGoldPrice];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            if(completion)
            {
                NSArray *listGold = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                objectGold = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listGold objectAtIndex:0]];
                completion();
            }
            
        }
        else
        {
            [hudView hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)reqOil :(void (^)(void))completion {
    
    [hudView show:YES];
    RequestModelADV *service = [[RequestModelADV alloc]initGetOilPrice];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            if(completion)
            {
                NSArray *listOil = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                objectOil = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listOil objectAtIndex:0]];
                completion();
            }
            
        }
        else
        {
            [hudView hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)reqLotto :(void (^)(void))completion {
    
    [hudView show:YES];
    RequestModelADV *service = [[RequestModelADV alloc]initGetLotteryResults];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            if(completion)
            {
                NSArray *listLotto = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                objectLotto = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLotto objectAtIndex:0]];
                completion();
            }
            
        }
        else
        {
            [hudView hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)changeClicked:(id)sender {
    if([sender tag]==0)
    {
        [self.goldButton setBackgroundImage: [UIImage imageNamed:@"button10-1.png"] forState:UIControlStateNormal];
        [self.oilButton setBackgroundImage:[UIImage imageNamed:@"button11-2.png"] forState:UIControlStateNormal];
        [self.lottoButton setBackgroundImage:[UIImage imageNamed:@"button12-2.png"] forState:UIControlStateNormal];
        
        [self.viewGold setHidden:NO];
        [self.viewOil setHidden:YES];
        [self.viewLotto setHidden:YES];
        
    }else if([sender tag]==1)
    {
        [self.goldButton setBackgroundImage:[UIImage imageNamed:@"button10-2.png"] forState:UIControlStateNormal];
        [self.oilButton setBackgroundImage:[UIImage imageNamed:@"button11-1.png"] forState:UIControlStateNormal];
        [self.lottoButton setBackgroundImage:[UIImage imageNamed:@"button12-2.png"] forState:UIControlStateNormal];
        
        [self.viewGold setHidden:YES];
        [self.viewOil setHidden:NO];
        [self.viewLotto setHidden:YES];
        
    }else if([sender tag]==2)
    {
        [self.goldButton setBackgroundImage:[UIImage imageNamed:@"button10-2.png"] forState:UIControlStateNormal];
        [self.oilButton setBackgroundImage:[UIImage imageNamed:@"button11-2.png"] forState:UIControlStateNormal];
        [self.lottoButton setBackgroundImage:[UIImage imageNamed:@"button12-1.png"] forState:UIControlStateNormal];
        
        
        [self.viewGold setHidden:YES];
        [self.viewOil setHidden:YES];
        [self.viewLotto setHidden:NO];
    }
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ข้อมูลอัพเดท"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end

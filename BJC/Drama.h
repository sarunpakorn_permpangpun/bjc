//
//  Drama.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Drama : UIViewController

@property (strong, nonatomic) NSString *slug;
@property (strong, nonatomic) NSString *nameDrama;

@property (strong, nonatomic) NSString *tvChanel;
@property (weak, nonatomic) IBOutlet UITableView *tableDramaSub;

- (IBAction)backClicked:(id)sender;
- (IBAction)homeClicked:(id)sender;


@end

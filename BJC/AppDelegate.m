//
//  AppDelegate.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 7/23/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "RequestModel.h"
#import <Google/Analytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    // Override point for customization after application launch. 
//    return YES;
//}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Let the device know we want to receive push notifications
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    // getDefaultTracker returns nil.
    self.tracker = [[GAI sharedInstance] defaultTracker];
    
    // Enable Advertising Features.
    [self.tracker setAllowIDFACollection:YES];
    
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        NSLog(@"iOS 8 Notifications");
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
    NSString *replaceString = [[NSString stringWithFormat:@"%@", deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@" "];
    replaceString = [[NSString stringWithFormat:@"%@", replaceString] stringByReplacingOccurrencesOfString:@"<" withString:@" "];
    replaceString = [[NSString stringWithFormat:@"%@", replaceString] stringByReplacingOccurrencesOfString:@">" withString:@" "];
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init];
    [userDefaults setObject:replaceString forKey:@"token"];
    [userDefaults synchronize];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    NSLog(@">>>>.. %@",url);
    NSLog(@">> .. %@ -- %@ -- %@",[url scheme], [url host], [url query]);
    
    if ( [[url scheme] isEqualToString:@"bjcapp"]) {
       
        NSLog(@"sheme : %@",[url scheme]);
        
        _pageid = [url host];
        NSLog(@"_pageid : %@",_pageid);

        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"tranferID" object:nil];
        
        return TRUE;
    }
    
    
  
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    NSLog(@"didReceiveRemoteNotification: %@", userInfo);
//
//    NSDictionary *objectUser = [[NSDictionary alloc]initWithDictionary:[userInfo objectForKey:@"aps"]];
//    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[objectUser objectForKey:@"badge"]integerValue];
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC"
//                                                    message:[NSString stringWithFormat:@"%@", [objectUser objectForKey:@"alert"]]
//                                                   delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
    
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler {
    
    NSDictionary *objectUser = [[NSDictionary alloc]initWithDictionary:[userInfo objectForKey:@"aps"]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[objectUser objectForKey:@"badge"]integerValue];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC"
                                                    message:[NSString stringWithFormat:@"%@", [objectUser objectForKey:@"alert"]]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init];
    
    RequestModel *serviceCountNoti = [[RequestModel alloc]initGetCountNotificationWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]];
    [serviceCountNoti requestDataWitnIndecator:^{
        if(serviceCountNoti.status)
        {
            NSString *countNotiBadge = [serviceCountNoti.jsonData objectForKey:@"count"];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = [countNotiBadge integerValue];
        }
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

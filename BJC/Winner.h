//
//  Winner.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 1/20/16.
//  Copyright © 2016 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Winner : UIViewController

@property (strong, nonatomic) NSDictionary *dicLucky;
@property (nonatomic) BOOL isFromViewNotification;

@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

- (IBAction)backClicked:(id)sender;

@end

//
//  NewsDescription.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/4/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "NewsDescription.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCopying.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKShareKit/FBSDKAppInviteContent.h>

#import <FBSDKShareKit/FBSDKShareDialogMode.h>
#import <FBSDKShareKit/FBSDKSharing.h>
#import <FBSDKShareKit/FBSDKSharingContent.h>

#import <FacebookSDK/FacebookSDK.h>
#import <Google/Analytics.h>

@interface NewsDescription ()

@end
@implementation NewsDescription

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *thumbnail = @"";
    NSObject *objectThumbnail = [self.dicNews objectForKey:@"fullimage"];
    if(objectThumbnail) {
        thumbnail = [NSString stringWithFormat:@"%@",  objectThumbnail];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
            NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail] options:NSDataReadingUncached error:nil];
            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                
                UIImage *myIcon = [UIImage imageWithData:dataImage];
                [self.imageView setImage:myIcon];
                //                [self.imageView setContentMode:UIViewContentModeScaleToFill];
            });
        });
    }
    //    else{
    //        [self.titleTextView setFrame:CGRectMake(8, self.titleTextView.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, self.titleTextView.frame.size.height)];
    //        [self.dateLabel setFrame:CGRectMake(8, self.dateLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, self.dateLabel.frame.size.height)];
    //    }
    
    [self.titleTextView setText:[NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"title"]]];
    [self.titleTextView sizeToFit];
    self.dateLabel.frame = CGRectMake(self.dateLabel.frame.origin.x, self.titleTextView.frame.origin.y+self.titleTextView.frame.size.height+8, self.dateLabel.frame.size.width, self.dateLabel.frame.size.height);
    [self.dateLabel setText:[NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"date"]]];
    self.viewLine.frame = CGRectMake(self.viewLine.frame.origin.x, self.dateLabel.frame.origin.y+self.dateLabel.frame.size.height+8, self.viewLine.frame.size.width, self.viewLine.frame.size.height);
    
    
    NSString *contentString = [NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"content"] ];
    if(![contentString isEqualToString:@""]) {
        //        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        //        [attributedString addAttribute:NSFontAttributeName
        //                                 value:[UIFont systemFontOfSize:14.0]
        //                                 range:NSMakeRange(0, attributedString.length)];
        [self.contentWebView loadHTMLString:contentString baseURL:nil];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.contentWebView.scrollView.scrollEnabled = NO;
    self.contentWebView.scrollView.bounces = NO;
    CGRect frame = self.contentWebView.frame;
    
    frame.size.height = 1;
    frame.origin.y = self.viewLine.frame.origin.y+self.viewLine.frame.size.height+8;
    self.contentWebView.frame = frame;
    
    frame.size.height = self.contentWebView.scrollView.contentSize.height;
    
    self.contentWebView.frame = frame;
    
    [self.scrollView setContentSize:CGSizeMake(self.contentWebView.frame.size.width, self.contentWebView.frame.origin.y+self.contentWebView.frame.size.height)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)facebookClicked:(id)sender {
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    NSString *string = [NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"id"]];
    NSData *strData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [strData base64EncodedStringWithOptions:0];
    
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://bjc.shinee.com/bjc/downloadapp?id=%@",base64String]];
    content.contentTitle = [NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"title"]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[[NSString stringWithFormat:@"%@", [self.dicNews objectForKey:@"content"]] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:14.0]
                             range:NSMakeRange(0, attributedString.length)];
    content.contentDescription = [NSString stringWithFormat:@"%@", [attributedString string]];
    
    NSURL *imageURL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@",[self.dicNews objectForKey:@"thumbnail"]]];
    content.imageURL = imageURL;
    
    FBSDKShareButton *button = [[FBSDKShareButton alloc] init];
    button.shareContent = content;
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        
        [button sendActionsForControlEvents: UIControlEventTouchUpInside];
        
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString *nameScreenName = [NSString stringWithFormat:@"ข่าวสาร - %@", [self.dicNews objectForKey:@"title"]];
    [tracker set:kGAIScreenName value:nameScreenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults :(NSDictionary *)results {
    NSLog(@"FB: SHARE RESULTS=%@\n",[results debugDescription]);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"FB: ERROR=%@\n",[error debugDescription]);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"FB: CANCELED SHARER=%@\n",[sharer debugDescription]);
}

@end

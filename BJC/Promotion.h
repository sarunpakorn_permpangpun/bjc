//
//  Promotion.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Promotion : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tablePromotion;
@property (weak, nonatomic) IBOutlet UITableView *tableOther;

@property (weak, nonatomic) IBOutlet UIView *viewPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *buttonPromotion;
@property (weak, nonatomic) IBOutlet UIButton *buttonNotification;
@property (weak, nonatomic) IBOutlet UIButton *buttonHowto;

@property (weak, nonatomic) IBOutlet UILabel *warnLabel;

- (IBAction)promotionClicked:(id)sender;
- (IBAction)notificationClicked:(id)sender;
- (IBAction)searchClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)marketClicked:(id)sender;
- (IBAction)returnClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)howtoClicked:(id)sender;


@end

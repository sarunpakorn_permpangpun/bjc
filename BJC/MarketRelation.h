//
//  MarketRelation.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/7/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketRelation : UIViewController  <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableMarket;
@property (weak, nonatomic) IBOutlet UISearchBar *textSearch;
@property (strong, readwrite) NSString *coupon_id;

- (IBAction)backClicked:(id)sender;
- (IBAction)okClicked:(id)sender;

@end

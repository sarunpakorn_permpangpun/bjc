//
//  EditProfile.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "EditProfile.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface EditProfile ()
{
    MBProgressHUD *hudView;
    
    NSMutableArray *listTambons, *listAmphurs, *listProvice;
    
    NSString *gender;
    int isTypePicker;
    
    NSMutableArray *listForPicker;
    NSUserDefaults *userDefaults;
    NSString *pickerSelectString;
    
    AppDelegate *appDelegate;
    
    NSMutableDictionary *dicProfileSeconed;
    
    NSMutableArray *listData, *listDataReplace;
}
@end

@implementation EditProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listData = [[NSMutableArray alloc]init];
    
    listDataReplace = [[NSMutableArray alloc]init];
    
    
    [self.viewPicker setHidden:YES];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    [numberToolbar setBarStyle:UIBarStyleDefault];
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"ปิด" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
//    _ageTextField.inputAccessoryView = numberToolbar;
    _zipcodeTextField.inputAccessoryView = numberToolbar;
    _telTextField.inputAccessoryView = numberToolbar;
    [self.scrollInputData setContentSize:CGSizeMake(_scrollInputData.frame.size.width, _viewInputData.frame.size.height)];
    
    userDefaults = [[NSUserDefaults alloc]init];
    
    
    listTambons = [[NSMutableArray alloc]initWithArray:(NSArray *)[userDefaults objectForKey:@"listTambons"]];
    
    listAmphurs = [[NSMutableArray alloc]initWithArray:(NSArray *)[userDefaults objectForKey:@"listAmphurs"]];
    
    listProvice = [[NSMutableArray alloc]initWithArray:(NSArray *)[userDefaults objectForKey:@"listProvice"]];
    
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    
    [self.view addSubview:hudView];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    dicProfileSeconed = [[NSMutableDictionary alloc]initWithDictionary:appDelegate.dicProfile];
    
    self.addressTextField.text = [appDelegate.dicProfile objectForKey:@"address"];
    self.ageTextField.text = [appDelegate.dicProfile objectForKey:@"age"];
    self.districtTextField.text = [appDelegate.dicProfile objectForKey:@"district"];
    self.emailTextField.text = [appDelegate.dicProfile objectForKey:@"email"];
    self.firstnameTextField.text = [appDelegate.dicProfile objectForKey:@"name"];
    self.proviceTextField.text = [appDelegate.dicProfile objectForKey:@"province"];
    self.passwordTextField.text = [appDelegate.dicProfile objectForKey:@"pw"];
    self.roadTextField.text = [appDelegate.dicProfile objectForKey:@"road"];
    self.subdistrictTextField.text = [appDelegate.dicProfile objectForKey:@"sub-district"];
    self.surnnameTextField.text = [appDelegate.dicProfile objectForKey:@"surname"];
    self.telTextField.text = [appDelegate.dicProfile objectForKey:@"tel"];
    self.usernameTextField.text = [appDelegate.dicProfile objectForKey:@"user"];
    self.zipcodeTextField.text = [appDelegate.dicProfile objectForKey:@"zipcode"];
    
    gender = [appDelegate.dicProfile objectForKey:@"gender"];
    if([gender isEqualToString:@"m"])
    {
            [_maleButton setBackgroundImage:[UIImage imageNamed:@"dot2.png"] forState:UIControlStateNormal];
            [_femaleButton setBackgroundImage:[UIImage imageNamed:@"dot1.png"] forState:UIControlStateNormal];
    }else if([gender isEqualToString:@"f"]){
            [_maleButton setBackgroundImage:[UIImage imageNamed:@"dot1.png"] forState:UIControlStateNormal];
            [_femaleButton setBackgroundImage:[UIImage imageNamed:@"dot2.png"] forState:UIControlStateNormal];
    }
    //gender
    NSLog(@"%@", appDelegate.dicProfile);
}

-(void)doneWithNumberPad{
    [_ageTextField resignFirstResponder];
    [_zipcodeTextField resignFirstResponder];
    [_telTextField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)validate {
    
    if(self.usernameTextField.text.length==0) {
        return NO;
    }else if(self.passwordTextField.text.length==0) {
        return NO;
    }else if(self.firstnameTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกชื่อ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.surnnameTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกนามสกุล" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if([gender isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณาเลือกเพศ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if([self.ageTextField.text integerValue]==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกปีเกิด" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.addressTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกที่อยู่" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.subdistrictTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกแขวง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.districtTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกเขต" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.proviceTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกจังหวัด" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.zipcodeTextField.text.length!=5) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกรหัสไปรษณีย์" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.emailTextField.text.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกอีกเมล์" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
        
    }else if(self.emailTextField.text.length > 0 && ![self validateEmailWithString:self.emailTextField.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"รูปแบบอีเมล์ไม่ถูดต้อง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }else if(self.telTextField.text.length!=10) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรุณากรอกเบอร์โทรศัพท์" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return NO;
        
    }
    
    return YES;
}

- (IBAction)saveClicked:(id)sender {
    
    @try {
        [hudView show:YES];
        if([self validate])
        {
            RequestModel *service = [[RequestModel alloc]initEditProfileWithUsername:[_usernameTextField text] Password:[_passwordTextField text] Firstname:[_firstnameTextField text] Surname:[_surnnameTextField text] Age:[_ageTextField text] Gender:gender Email:[_emailTextField text] Tel:[_telTextField text] Address:[_addressTextField text] Road:[_roadTextField text] Subdistrict:[_subdistrictTextField text] District:[_districtTextField text] Province:[_proviceTextField text] Zipcode:[_zipcodeTextField text]];
          
            [service requestDataWitnIndecator:^{ [hudView hide:YES]; [hudView removeFromSuperview];
                if(service.status)
                {
                    [dicProfileSeconed setObject:self.addressTextField.text forKey:@"address"];
                    [dicProfileSeconed setObject:self.ageTextField.text forKey:@"age"];
                    [dicProfileSeconed setObject:self.districtTextField.text forKey:@"district"];
                    [dicProfileSeconed setObject:self.emailTextField.text forKey:@"email"];
                    [dicProfileSeconed setObject:self.firstnameTextField.text forKey:@"name"];
                    [dicProfileSeconed setObject:self.proviceTextField.text forKey:@"province"];
                    [dicProfileSeconed setObject:self.roadTextField.text forKey:@"road"];
                    [dicProfileSeconed setObject:self.subdistrictTextField.text forKey:@"sub-district"];
                    [dicProfileSeconed setObject:self.surnnameTextField.text forKey:@"surname"];
                    [dicProfileSeconed setObject:self.telTextField.text forKey:@"tel"];
                    [dicProfileSeconed setObject:self.zipcodeTextField.text forKey:@"zipcode"];
                    [dicProfileSeconed setObject:gender forKey:@"gender"];
                    
                    appDelegate.dicProfile = [[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)dicProfileSeconed];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }];
            
        }else {
             [hudView hide:YES];
        }
        
    }
    @catch (NSException *exception) {
        [hudView hide:YES];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"กรอกข้อมูลไม่ถูกต้อง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
    }
}

- (IBAction)bgtab:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)selectorYear:(id)sender{
    
    [self.view endEditing:YES];
    isTypePicker = 4;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int formatter_year = [[formatter stringFromDate:[NSDate date]] intValue];
    int year = (formatter_year < 2559)? formatter_year+543 : formatter_year;
    
    NSLog(@"year = %d",year);
    
    
    listForPicker = [[NSMutableArray alloc] init];
    
    for (int i = 2459 ; i<=year; i++) {
        
        [listForPicker addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [_picker reloadAllComponents];
    [_picker selectRow:0 inComponent:0 animated:YES];
    
    if(listForPicker.count>0) {
        pickerSelectString = [NSString stringWithFormat:@"%@", [listForPicker objectAtIndex:0]];
    }
    
    [self showPickerView];
    
}


- (IBAction)genderClicked:(id)sender {
    if([sender tag]==0){
        [_maleButton setBackgroundImage:[UIImage imageNamed:@"dot2.png"] forState:UIControlStateNormal];
        [_femaleButton setBackgroundImage:[UIImage imageNamed:@"dot1.png"] forState:UIControlStateNormal];
        gender = @"m";
    }else if([sender tag]==1){
        [_maleButton setBackgroundImage:[UIImage imageNamed:@"dot1.png"] forState:UIControlStateNormal];
        [_femaleButton setBackgroundImage:[UIImage imageNamed:@"dot2.png"] forState:UIControlStateNormal];
        gender = @"f";
    }
}

- (IBAction)subdistrictClicked:(id)sender {
    [self.view endEditing:YES];
    UIAlertView *message;
    if([_proviceTextField text].length==0)
    {
        
        message = [[UIAlertView alloc] initWithTitle:@""
                                             message:@"กรุณาเลือกจังหวัด"
                                            delegate:nil
                                   cancelButtonTitle:@"ตกลง"
                                   otherButtonTitles:nil];
        [message show];
    }else if([_districtTextField text].length==0)
    {
        
        message = [[UIAlertView alloc] initWithTitle:@""
                                             message:@"กรุณาเลือกเขต"
                                            delegate:nil
                                   cancelButtonTitle:@"ตกลง"
                                   otherButtonTitles:nil];
        [message show];
    }else{
        isTypePicker = 3;
        pickerSelectString = @"";
        NSMutableDictionary *dicTambons = [[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)[userDefaults objectForKey:@"dicTambons"]];
        listForPicker = [[NSMutableArray alloc]initWithArray:(NSMutableArray *)[dicTambons objectForKey:[_districtTextField text]]];
        [_picker reloadAllComponents];
        [_picker selectRow:0 inComponent:0 animated:YES];
        
        if(listForPicker.count>0) {
            pickerSelectString = [NSString stringWithFormat:@"%@", [listForPicker objectAtIndex:0]];
        }
        
        [self showPickerView];
    }
}

- (IBAction)districtClicked:(id)sender {
    [self.view endEditing:YES];
    if([_proviceTextField text].length==0){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:@"กรุณาเลือกจังหวัด"
                                                         delegate:nil
                                                cancelButtonTitle:@"ตกลง"
                                                otherButtonTitles:nil];
        [message show];
    }else{
        isTypePicker = 2;
        pickerSelectString = @"";
        NSMutableDictionary *dicAmphurs = [[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)[userDefaults objectForKey:@"dicAmphurs"]];
        listForPicker = [[NSMutableArray alloc]initWithArray:(NSMutableArray *)[dicAmphurs objectForKey:[_proviceTextField text]]];
        [_picker reloadAllComponents];
        [_picker selectRow:0 inComponent:0 animated:YES];
        
        if(listForPicker.count>0) {
            pickerSelectString = [NSString stringWithFormat:@"%@", [listForPicker objectAtIndex:0]];
        }
        
        [self showPickerView];
    }
}

- (IBAction)proviceClicked:(id)sender {
    [self.view endEditing:YES];
    isTypePicker = 1;
    pickerSelectString = @"";
    listForPicker = [[NSMutableArray alloc]initWithArray:(NSMutableArray *)[userDefaults objectForKey:@"listProvice"]];
    [_picker reloadAllComponents];
    [_picker selectRow:0 inComponent:0 animated:YES];
    
    if(listForPicker.count>0) {
        pickerSelectString = [NSString stringWithFormat:@"%@", [listForPicker objectAtIndex:0]];
    }
    
    [self showPickerView];
}
- (IBAction)returnPicker:(id)sender {
    //    NSString *postcodeString;
    switch (isTypePicker) {
        case 1:
            [_proviceTextField setText:pickerSelectString];
            [_districtTextField setText:@""];
            [_subdistrictTextField setText:@""];
            break;
        case 2:
            [_districtTextField setText:pickerSelectString];
            [_subdistrictTextField setText:@""];
            break;
        case 3:
            [_subdistrictTextField setText:pickerSelectString];
            break;
        case 4:
            [_ageTextField setText:pickerSelectString];
            break;
        default:
            break;
    }
    [self hidePickerView];
}

- (IBAction)cancelPicker:(id)sender {
    [self hidePickerView];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    [self hidePickerView];
//    return YES;

    if(textField.tag>0 && textField.tag<4) {
        mTextDropDownListView = [[XDPopupListView alloc] initWithBoundView:textField dataSource:self delegate:self popupType:XDPopupListViewDropDown];
        
        [textField addTarget:self action:@selector(textDidChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return YES;
}

- (void)textDidChanged:(id)sender {
    UITextField *textField = (UITextField *)sender;
    
    NSString *trimmed = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![self isNullOrEmpty:trimmed]) {
        
//        if(trimmed.length == 1) {
        
            if(textField.tag == 3) {
                
                listDataReplace = [[NSMutableArray alloc]initWithArray:(NSArray*)listProvice];
                
            }else if(textField.tag == 2) {
                
                listDataReplace = [[NSMutableArray alloc]initWithArray:(NSArray*)listAmphurs];
                
            }else if(textField.tag == 1) {
                
                listDataReplace = [[NSMutableArray alloc]initWithArray:(NSArray*)listTambons];
            }
            
            [mTextDropDownListView reloadListData];
            
            [self searchTextForlist:textField.text];
            
            [mTextDropDownListView show];
            
//        }else {
//            
//            [self searchTextForlist:textField.text TagType:textField.tag];
//        }
    }else {
        [mTextDropDownListView dismiss];
    }
}

#pragma mark - XDPopupListViewDataSource & XDPopupListViewDelegate

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return listData.count;
}
- (CGFloat)itemCellHeight:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (void)clickedListViewAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *stringAddress = [NSString stringWithFormat:@"%@", [listData objectAtIndex:indexPath.row]];
    
    NSArray *listSubAddress = [stringAddress componentsSeparatedByString:@","];
    
    if(listSubAddress.count == 3) {
        
        self.subdistrictTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:0]];
        
        self.districtTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:1]];
        
        self.proviceTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:2]];
        
    }else if(listSubAddress.count == 2) {
        
        self.districtTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:0]];
        
        self.proviceTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:1]];
        
    }else if(listSubAddress.count == 1) {
        
        self.proviceTextField.text = [NSString stringWithFormat:@"%@", [listSubAddress objectAtIndex:0]];
        
    }
    else {
        
        self.subdistrictTextField.text = @"";
        
        self.districtTextField.text = @"";
        
        self.proviceTextField.text = @"";
    }

}

- (UITableViewCell *)itemCell:(NSIndexPath *)indexPath
{
    if (listData.count == 0) {
        return nil;
    }
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.textLabel.text = listData[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return cell;
}

- (void)searchTextForlist:(NSString *)str
{
    [listData removeAllObjects];
    for(NSString *curString in listDataReplace) {
        NSRange substringRange = [curString rangeOfString:str];
        if (substringRange.location != NSNotFound) {
            [listData addObject:curString];
        }
    }
    
    listDataReplace = [[NSMutableArray alloc]initWithArray:(NSArray*)listData];
    
    if(![mTextDropDownListView isShowing]) {
        [mTextDropDownListView show];
        if(listData.count>0) {
            [mTextDropDownListView show];
        }else {
            [mTextDropDownListView dismiss];
        }
    }else if(listData.count>0) {
        [mTextDropDownListView show];
    }else {
        [mTextDropDownListView dismiss];
    }
    [mTextDropDownListView reloadListData];
}

- (BOOL)isNullOrEmpty:(NSString *)s {
    if(s == nil ||
       s.length == 0 ||
       [s isEqualToString:@""] ||
       [s isEqualToString:@"(null)"])
    {
        return YES;
    }
    
    return NO;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    pickerSelectString = [[NSString alloc]init];
    pickerSelectString = [NSString stringWithFormat:@"%@", [listForPicker objectAtIndex:row]];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [listForPicker count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 46)];
    [label setText:[NSString stringWithFormat:@"       %@", [listForPicker objectAtIndex:row]]];
    label.textAlignment = NSTextAlignmentCenter; //Changed to NS as UI is deprecated.
    label.backgroundColor = [UIColor clearColor];
    [label setTextColor:[UIColor whiteColor]];
    
    return label;
}

- (void)showPickerView {
    [self.viewPicker setHidden:NO];
    [UIView animateWithDuration:0.75
                     animations:^{
                         [_viewPicker setFrame: CGRectMake(0, heightScreen-_viewPicker.frame.size.height, widthScreen, _viewPicker.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)hidePickerView {
    [UIView animateWithDuration:0.75
                     animations:^{
                         [_viewPicker setFrame: CGRectMake(0, heightScreen, widthScreen, _viewPicker.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                         isTypePicker=0;
                         [self.viewPicker setHidden:YES];
                     }];
}


-(IBAction)homeClicked:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self hidePickerView];
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const float movementDuration = 0.3f;
    
    float keyboardHeight = 380;
    float textFieldTopToBottom = textField.frame.origin.y + textField.frame.size.height;
    float textFieldOnOrUnder =  heightScreen - textFieldTopToBottom;
    int movementDistance = (textFieldTopToBottom-keyboardHeight);
    
    
    
    if(heightScreen<=480) {
        
        if(textField.tag > 0 && textField.tag < 4) {
            
            movementDistance+=160;
            
        }else {
            
            movementDistance+=115;
        }
        
    }else if(heightScreen<=568) {
        
        if(textField.tag > 0 && textField.tag < 4) {
            
            movementDistance+=130;
            
        }else {
            
            movementDistance+=85;
        }
        
    }else if(heightScreen<=667) {
        
        if(textField.tag > 0 && textField.tag < 4) {
            movementDistance+=50;
        }
        
    }else if(heightScreen<=736) {
        
        if(textField.tag > 0 && textField.tag < 4) {
            movementDistance+=30;
        }else {
            movementDistance-=80;
        }
        
    }else{
        movementDistance-=(keyboardHeight/1.87);
    }
    

    if(keyboardHeight>textFieldOnOrUnder && movementDistance>0)
    {
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(textField.tag == 99) {
        return newLength <= 3;
    }else if(textField.tag == 98) {
        return newLength <= 5;
    }else if(textField.tag == 97) {
        return newLength <= 10;
    }else {
        return YES;
    }
}

- (BOOL)validateEmailWithString:(NSString*)text
{
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:text];
}

@end


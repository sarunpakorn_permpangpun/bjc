//
//  EntertainmentDescription.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/16/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "EntertainmentDescription.h"

@interface EntertainmentDescription ()

@end
@implementation EntertainmentDescription

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [self.dicEntertainment objectForKey:@"fullimage"]];
    if(![thumbnail isEqualToString:@""])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
            NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail] options:NSDataReadingUncached error:nil];
            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                
                UIImage *myIcon = [UIImage imageWithData:dataImage];
                [self.imageView setImage:myIcon];
                [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
            });
        });
    }else{
        [self.dateLabel setFrame:CGRectMake(8, self.dateLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, self.dateLabel.frame.size.height)];
    }
    
    [self.titleTextView setText:[NSString stringWithFormat:@"%@", [self.dicEntertainment objectForKey:@"title"]]];
    [self.dateLabel setText:[NSString stringWithFormat:@"%@", [self.dicEntertainment objectForKey:@"date"]]];
    
    NSString *contentString = [NSString stringWithFormat:@"%@", [self.dicEntertainment objectForKey:@"content"] ];
    if(![contentString isEqualToString:@""]) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont systemFontOfSize:14.0]
                                 range:NSMakeRange(0, attributedString.length)];
        self.contentTextView.attributedText = attributedString;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end

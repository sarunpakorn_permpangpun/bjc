//
//  Display.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 7/23/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Display.h"
#import "AppDelegate.h"
#import "Login.h"
#import "RequestModel.h"

@interface Display ()

@end

@implementation Display

- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    NSLog(@"%f", heightScreen);
    if(heightScreen==480)
    {
        [self.imageViewDisplay setImage:[UIImage imageNamed:@"i4_splash.png"]];
        
    }else if(heightScreen==568)
    {
        [self.imageViewDisplay setImage:[UIImage imageNamed:@"i5_splash.png"]];
        
    }else if(heightScreen==667)
    {
        [self.imageViewDisplay setImage:[UIImage imageNamed:@"i6_splash.png"]];
        
    }else if(heightScreen==736)
    {
        [self.imageViewDisplay setImage:[UIImage imageNamed:@"i6plus_splash.png"]];
    }else{
        [self.imageViewDisplay setImage:[UIImage imageNamed:@"ipad_splash.png"]];
    }
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init];
    
    if(![userDefaults objectForKey:@"listProvice"])
    {
        NSMutableArray *listProvice = [[NSMutableArray alloc]init];
        
        NSMutableArray *listAmphurs = [[NSMutableArray alloc]init];
        
        NSMutableArray *listTambons = [[NSMutableArray alloc]init];
        
        
        
        //        NSMutableArray *listProvice = [[NSMutableArray alloc]init];
        //        NSMutableDictionary *dicAmphurs = [[NSMutableDictionary alloc]init];
        //        NSMutableDictionary *dicTambons = [[NSMutableDictionary alloc]init];
        
        RequestModel *service = [[RequestModel alloc]initGetCCAATT];
        [service requestDataWitnIndecator:^{
            if(service.status)
            {
                NSMutableArray *listResult = [[NSMutableArray alloc]initWithArray:[service.jsonData objectForKey:@"rows"]];
                
                //                for(NSDictionary *dicResult in listResult){
                //
                //                    NSMutableArray *listAmphurs = [[NSMutableArray alloc]initWithArray:(NSArray *)[dicResult objectForKey:@"amphurs"]];
                //                    NSMutableArray *amphursarray = [[NSMutableArray alloc]init];
                //
                //                    for(NSDictionary *amphurs in listAmphurs){
                //
                //                        [amphursarray addObject:[amphurs objectForKey:@"name"]];
                //
                //                        NSMutableArray *listtambons = [[NSMutableArray alloc]initWithArray:(NSArray *)[amphurs objectForKey:@"tambons"]];
                //                        NSMutableArray *tambonsarray = [[NSMutableArray alloc]init];
                //                        for(NSDictionary *tambons in listtambons){
                //                            [tambonsarray addObject:[tambons objectForKey:@"name"]];
                //                        }
                //                        [dicTambons setObject:tambonsarray forKey:[NSString stringWithFormat:@"%@", [amphurs objectForKey:@"name"]]]; // NAME TAMBONS
                //                    }
                //
                //                    [dicAmphurs setObject:amphursarray forKey:[NSString stringWithFormat:@"%@", [dicResult objectForKey:@"name"]]]; // NAME AMPHURS & POST CODE
                //
                //                    [listProvice addObject:[NSString stringWithFormat:@"%@", [dicResult objectForKey:@"name"]]]; // NAME PROVICE
                //                }
                //
                //                [userDefaults setObject:listProvice forKey:@"listProvice"];
                //                [userDefaults setObject:dicAmphurs forKey:@"dicAmphurs"];
                //                [userDefaults setObject:dicTambons forKey:@"dicTambons"];
                //                [userDefaults synchronize];
                
                
                for(NSDictionary *dicResult in listResult){
                    
                    NSString *stringAddress = [NSString stringWithFormat:@"%@", [dicResult objectForKey:@"name"]];
                    
                    [listProvice addObject:[NSString stringWithFormat:@"%@", stringAddress]];
                    
                    
                    NSMutableArray *listAmphurs_ = [[NSMutableArray alloc]initWithArray:(NSArray *)[dicResult objectForKey:@"amphurs"]];
                    
                    for(NSDictionary *amphurs in listAmphurs_){
                        
                        NSString *stringAddress = [NSString stringWithFormat:@"%@,%@", [amphurs objectForKey:@"name"], [dicResult objectForKey:@"name"]];
                        
                        [listAmphurs addObject:[NSString stringWithFormat:@"%@", stringAddress]];
                        
                        
                        NSMutableArray *listtambons_ = [[NSMutableArray alloc]initWithArray:(NSArray *)[amphurs objectForKey:@"tambons"]];
                        
                        for(NSDictionary *tambons in listtambons_){
                            
                            NSString *stringAddress = [NSString stringWithFormat:@"%@,%@,%@", [tambons objectForKey:@"name"], [amphurs objectForKey:@"name"], [dicResult objectForKey:@"name"]];
                            
                            [listTambons addObject:[NSString stringWithFormat:@"%@", stringAddress]];
                        }
                        
                    }
                }
                
                [userDefaults setObject:listTambons forKey:@"listTambons"];
                [userDefaults setObject:listAmphurs forKey:@"listAmphurs"];
                [userDefaults setObject:listProvice forKey:@"listProvice"];
                
                [userDefaults synchronize];
                
                
                if([userDefaults synchronize]) {
                    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(goToLogin) userInfo:nil repeats:NO];
                    //                    [self goToLogin];
                }
                
            }else{
                NSLog(@"ERROR #GetCCAATT: %@", service.errorMessage);
            }
        }];
    }else{
        [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(goToLogin) userInfo:nil repeats:NO];
        //        [self goToLogin];
    }
}

- (void)goToLogin {
    Login *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
    UINavigationController *loginNC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    loginNC.navigationBarHidden = YES;
    [self presentViewController:loginNC animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

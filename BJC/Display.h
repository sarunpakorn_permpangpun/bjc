//
//  Display.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 7/23/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Display : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageViewDisplay;

@end

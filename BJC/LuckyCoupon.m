//
//  LuckyCoupon.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "LuckyCoupon.h"
#import "MBProgressHUD.h"
#import "RequestModel.h"
#import "MarketRelation.h"

@interface LuckyCoupon ()
{
    BOOL keyboardIsShown;
}
@end
@implementation LuckyCoupon

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.maketRelationButton.layer setCornerRadius:10.0f];
    [self.imageBg.layer setCornerRadius:10.0f];
    [self.imageBg.layer setBorderWidth:1.0f];
    [self.imageBg.layer setBorderColor:[[UIColor lightGrayColor]CGColor]];
    [self.idMaketTextField.layer setBorderColor:[[UIColor blackColor]CGColor]];
    [self.idMaketTextField.layer setBorderWidth:1.0f];
    [self.idMaketTextField.layer setCornerRadius:10.0f];
    
    self.messageLabel.text = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"message"]];
    
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"thumbnail"]];
    if([thumbnail isEqualToString:@""])
    {
        thumbnail = @"";
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
        NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail] options:NSDataReadingUncached error:nil];
        dispatch_sync(dispatch_get_main_queue(), ^(void) {
            
            UIImage *myIcon = [UIImage imageWithData:dataImage];
            [self.imageView setImage:myIcon];
        });
    });
    
    [self.serialCouponTextField setText:[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"code"]]];
    [self.viewInputCoupon setHidden:![[self.dicLucky objectForKey:@"can_use"] boolValue]];
    
    if([self.viewInputCoupon isHidden]) {
        
         [_messageLabel setBackgroundColor:[UIColor redColor]];
        
        self.viewCoupon.hidden = NO;
        CGRect frameScrollView = self.scrollView.frame;
        frameScrollView.origin.y = 239;
        frameScrollView.size.height = [UIScreen mainScreen].bounds.size.height - 368;
        self.scrollView.frame = frameScrollView;
        self.scrollView.contentOffset = CGPointMake(0, 0);

        
    }else {
        
        self.viewCoupon.hidden = YES;
        CGRect frameScrollView = self.scrollView.frame;
        frameScrollView.origin.y = 158;
        frameScrollView.size.height = [UIScreen mainScreen].bounds.size.height - 239;
        self.scrollView.frame = frameScrollView;
        self.scrollView.contentOffset = CGPointMake(0, 0);
        
    }
    
    NSString *contentString = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"content"] ];
    if(![contentString isEqualToString:@""]) {
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//        [attributedString addAttribute:NSFontAttributeName
//                                 value:[UIFont systemFontOfSize:16.0f]
//                                 range:NSMakeRange(0, attributedString.length)];
//        self.conditionTextView.attributedText = attributedString;
        
        [self.conditionWebView loadHTMLString:contentString baseURL:nil];
       
    }else {
        [self.conditionLabel setHidden:YES];
        [self.conditionWebView setHidden:YES];
        [self.dateLabel setFrame:CGRectMake(self.dateLabel.frame.origin.x, self.conditionLabel.frame.origin.y, self.dateLabel.frame.size.width, self.dateLabel.frame.size.height)];
        
        [self.dateTextView setText:[NSString stringWithFormat:@"%@ - %@", [self.dicLucky objectForKey:@"valid_from"], [self.dicLucky objectForKey:@"valid_to"]]];
        [self.dateTextView setFrame:CGRectMake(self.dateTextView.frame.origin.x, self.dateLabel.frame.origin.y+self.dateLabel.frame.size.height+8, self.dateTextView.frame.size.width, self.dateTextView.frame.size.height)];
        [self.dateTextView setFont:[UIFont systemFontOfSize:16.0f]];
        
        [self.dateTextView sizeToFit];
        [self.maketRelationButton setFrame:CGRectMake(self.maketRelationButton.frame.origin.x, self.dateTextView.frame.origin.y+self.dateTextView.frame.size.height+32, self.maketRelationButton.frame.size.width, self.maketRelationButton.frame.size.height)];
        [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, self.maketRelationButton.frame.origin.y+self.maketRelationButton.frame.size.height+32)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.conditionWebView.scrollView.scrollEnabled = NO;
    self.conditionWebView.scrollView.bounces = NO;
    CGRect frame = self.conditionWebView.frame;
    
    frame.size.width = self.scrollView.frame.size.width;
    frame.size.height = 1;    self.conditionWebView.frame = frame;
    
    frame.size.height = self.conditionWebView.scrollView.contentSize.height;
    
    self.conditionWebView.frame = frame;
    
    
    [self.dateTextView setText:[NSString stringWithFormat:@"%@ - %@", [self.dicLucky objectForKey:@"valid_from"], [self.dicLucky objectForKey:@"valid_to"]]];
    [self.dateLabel setFrame:CGRectMake(self.dateLabel.frame.origin.x, self.conditionWebView.frame.origin.y+self.conditionWebView.frame.size.height+8, self.dateLabel.frame.size.width, self.dateLabel.frame.size.height)];
    [self.dateTextView setFrame:CGRectMake(self.dateTextView.frame.origin.x, self.dateLabel.frame.origin.y+self.dateLabel.frame.size.height+8, self.dateTextView.frame.size.width, self.dateTextView.frame.size.height)];
    [self.dateTextView setFont:[UIFont systemFontOfSize:16.0f]];
    [self.dateTextView sizeToFit];
    
    
    [self.maketRelationButton setFrame:CGRectMake(self.maketRelationButton.frame.origin.x, self.dateTextView.frame.origin.y+self.dateTextView.frame.size.height+32, self.maketRelationButton.frame.size.width, self.maketRelationButton.frame.size.height)];
    [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, self.maketRelationButton.frame.origin.y+self.maketRelationButton.frame.size.height+32)];

}
- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.view.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.origin.y += (keyboardSize.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.view.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.origin.y -= (keyboardSize.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)maketRelationClicked:(id)sender {
    
    MarketRelation *marketRelationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MarketRelation"];
    marketRelationVC.coupon_id = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"id"]];
//    [self.navigationController pushViewController:marketRelationVC animated:YES];
    marketRelationVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentModalViewController:marketRelationVC animated:YES];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okClicked:(id)sender {
    MBProgressHUD *hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initUseCouponWithCouponId:[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"id"]] CouponCode:[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"code"]] StoreCode:self.idMaketTextField.text];
    [service requestDataWitnIndecator:^{[hudView show:NO]; [hudView removeFromSuperview];
        if(service.status)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"ใช้คูปองเรียบร้อยแล้ว" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            alert.tag = 1;
//            [alert show];
            
            self.messageLabel.text = [NSString stringWithFormat:@"%@", [service.jsonData objectForKey:@"message"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadCoupon" object:nil];
            [_messageLabel setHidden:NO];
            [_viewInputCoupon setHidden:YES];
            
            
            self.viewCoupon.hidden = NO;
            CGRect frameScrollView = self.scrollView.frame;
            frameScrollView.origin.y = 239;
            frameScrollView.size.height = [UIScreen mainScreen].bounds.size.height - 368;
            self.scrollView.frame = frameScrollView;
            self.scrollView.contentOffset = CGPointMake(0, 0);
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];

}

- (IBAction)tapAction:(id)sender {
    [self.view endEditing:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if(alertView.tag==1)
        {
            [self.viewInputCoupon setHidden:YES];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
@end

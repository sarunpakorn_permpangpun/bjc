//
//  LuckyCampaign.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LuckyCampaign : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) NSDictionary *dicLucky;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UIWebView *conditionWebView;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *dateTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *checkLuckyButton;

- (IBAction)backClicked:(id)sender;
- (IBAction)checkLuckyClicked:(id)sender;
- (IBAction)facebookClicked:(id)sender;

@end

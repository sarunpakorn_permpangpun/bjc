//
//  LuckyCampaign.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "LuckyCampaign.h"
#import "AppDelegate.h"
#import "Winner.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCopying.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKShareKit/FBSDKAppInviteContent.h>

#import <FBSDKShareKit/FBSDKShareDialogMode.h>
#import <FBSDKShareKit/FBSDKSharing.h>
#import <FBSDKShareKit/FBSDKSharingContent.h>

#import <FacebookSDK/FacebookSDK.h>
#import <Google/Analytics.h>

@interface LuckyCampaign ()

@end
@implementation LuckyCampaign

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"fullimage"]];
    if(![thumbnail isEqualToString:@"(null)"] && ![thumbnail isEqualToString:@""])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
            NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail] options:NSDataReadingUncached error:nil];
            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                
                UIImage *myIcon = [UIImage imageWithData:dataImage];
                [self.imageView setImage:myIcon];
                [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
            });
        });
    }else{
        [self.titleTextView setFrame:CGRectMake(8, self.titleTextView.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, self.titleTextView.frame.size.height)];
        [self.dateLabel setFrame:CGRectMake(8, self.dateLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, self.dateLabel.frame.size.height)];
    }
    
    [self.titleTextView setText:[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"title"]]];

    BOOL can_use = [[self.dicLucky objectForKey:@"can_use"] boolValue];
    if(can_use) {
        [self.checkLuckyButton setEnabled:NO];
        [self.checkLuckyButton setBackgroundColor:color_gray];
    }
    [self.checkLuckyButton.layer setCornerRadius:7.0f];
    
    NSString *contentString = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"content"] ];
    if(![contentString isEqualToString:@""]) {
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//        [attributedString addAttribute:NSFontAttributeName
//                                 value:[UIFont systemFontOfSize:16.0]
//                                 range:NSMakeRange(0, attributedString.length)];
//        self.conditionTextView.attributedText = attributedString;
    [self.conditionWebView loadHTMLString:contentString baseURL:nil];
        
    }else {
        [self.conditionLabel setHidden:YES];
        [self.conditionWebView setHidden:YES];
        [self.dateLabel setFrame:CGRectMake(self.dateLabel.frame.origin.x, self.conditionLabel.frame.origin.y, self.dateLabel.frame.size.width, self.dateLabel.frame.size.height)];
        
        [self.dateTextView setText:[NSString stringWithFormat:@"%@ - %@", [self.dicLucky objectForKey:@"valid_from"], [self.dicLucky objectForKey:@"valid_to"]]];
        [self.dateTextView setFrame:CGRectMake(self.dateTextView.frame.origin.x, self.dateLabel.frame.origin.y+self.dateLabel.frame.size.height, self.dateTextView.frame.size.width, self.dateTextView.frame.size.height)];
        [self.dateTextView setFont:[UIFont systemFontOfSize:16.0f]];
        [self.dateTextView sizeToFit];
        [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, self.dateTextView.frame.origin.y+self.dateTextView.frame.size.height+32)];
    }

    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.conditionWebView.scrollView.scrollEnabled = NO;
    self.conditionWebView.scrollView.bounces = NO;
    
    CGRect frame = self.conditionWebView.frame;
    
    frame.size.width = self.scrollView.frame.size.width;
    frame.size.height = 1;    self.conditionWebView.frame = frame;
    
    frame.size.height = self.conditionWebView.scrollView.contentSize.height;
    
    self.conditionWebView.frame = frame;
    
    [self.dateTextView setText:[NSString stringWithFormat:@"%@ - %@", [self.dicLucky objectForKey:@"valid_from"], [self.dicLucky objectForKey:@"valid_to"]]];
    [self.dateLabel setFrame:CGRectMake(self.dateLabel.frame.origin.x, self.conditionWebView.frame.origin.y+self.conditionWebView.frame.size.height+8, self.dateLabel.frame.size.width, self.dateLabel.frame.size.height)];
    [self.dateTextView setFrame:CGRectMake(self.dateTextView.frame.origin.x, self.dateLabel.frame.origin.y+self.dateLabel.frame.size.height, self.dateTextView.frame.size.width, self.dateTextView.frame.size.height)];
    [self.dateTextView setFont:[UIFont systemFontOfSize:16.0f]];
    [self.dateTextView sizeToFit];
    [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, self.dateTextView.frame.origin.y+self.dateTextView.frame.size.height+32)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkLuckyClicked:(id)sender {
    
    Winner *winnerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Winner"];
    winnerVC.dicLucky = self.dicLucky;
    winnerVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:winnerVC animated:YES completion:nil];
}

- (IBAction)facebookClicked:(id)sender {
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    NSString *string = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"id"]];
    NSData *strData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [strData base64EncodedStringWithOptions:0];

    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://bjc.shinee.com/bjc/downloadapp?id=%@",base64String]];
    content.contentTitle = [NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"title"]];
    content.contentDescription = [NSString stringWithFormat:@"ระยะเวลาการรับรางวัล %@ - %@", [self.dicLucky objectForKey:@"valid_from"], [self.dicLucky objectForKey:@"valid_to"]];
    
    NSURL *imageURL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@",[self.dicLucky objectForKey:@"thumbnail"]]];
    content.imageURL = imageURL;
    
    FBSDKShareButton *button = [[FBSDKShareButton alloc] init];
    button.shareContent = content;
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        
        [button sendActionsForControlEvents: UIControlEventTouchUpInside];
        
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString *nameScreenName = [NSString stringWithFormat:@"ชิงโชค - %@", [self.dicLucky objectForKey:@"title"]];
    [tracker set:kGAIScreenName value:nameScreenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults :(NSDictionary *)results {
    NSLog(@"FB: SHARE RESULTS=%@\n",[results debugDescription]);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"FB: ERROR=%@\n",[error debugDescription]);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"FB: CANCELED SHARER=%@\n",[sharer debugDescription]);
}
@end

//
//  Promotion.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Promotion.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"
#import "Market.h"
#import "PromotionDescription.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "LuckyCampaign.h"
#import "LuckyCoupon.h"

#import <Google/Analytics.h>

@interface Promotion ()
{
    NSMutableArray *listbrand;
    NSMutableArray *listPromotion;
    NSMutableArray *listLucky;
    NSString *slug;
    NSString *itemID;
    NSString *namePromtoin;
    int countPromotion;
    
    MBProgressHUD *hudView;
    BOOL isLoader;
    
    BOOL isPromotion;
    
    NSUserDefaults *userDefaults;
    
    
    NSDictionary *dicBrand;
}
@end

@implementation Promotion



- (void)reqMemberByEmail : (void (^)(BOOL isSuscess))completion {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    RequestModel *serviceGetMemberByEmail = [[RequestModel alloc]initGetMemberByEmailWithUsername:[NSString stringWithFormat:@"%@", [appDelegate.dicProfile objectForKey:@"user"]]];
    
    [serviceGetMemberByEmail requestDataWitnIndecator:^{
        
        if(serviceGetMemberByEmail.status)
        {
            NSDictionary *objectResult = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)serviceGetMemberByEmail.jsonData];
            
            BOOL isData = [[objectResult objectForKey:@"data"] boolValue];
            
            if(isData) {
                
                self.warnLabel.text = @"ระบบได้ทำการแจกคูปองเรียบร้อยแล้ว โปรดรอรับคูปองส่วนลดในรอบถัดไป";
                
            }else {
                
                self.warnLabel.text = @"ต้องการรับคูปองส่วนลด กรุณากรอกข้อมูลลงทะเบียนของท่านให้ครบถ้วน";
            }

            completion(YES);
            
        }else {
            
            completion(NO);
        }
    }];
}

- (void)reqLuckys :(NSString *)brand : (void (^)(BOOL isSuscess))completion {
    
    RequestModel *service = [[RequestModel alloc]initGetLuckyDrawWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]] PerPage:@"-1" Page:@"1" Type:@"coupon" Brand:brand];
    
    [service requestDataWitnIndecator:^{
        
        isLoader = NO;
        
        if(service.status)
        {
            listLucky = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
            
            completion(YES);
        }
        else
        {
            completion(NO);
        }
        
    }];
}

- (void)reqPromotions: (NSString *)keyword :(NSString *)per_page :(NSString *)brand :(void (^)(void))completion {
    
    countPromotion++;
    
    RequestModel *service = [[RequestModel alloc]initGetPromotionsWithKeyword:@"" PerPage:per_page Page:[NSString stringWithFormat:@"%d", countPromotion] Brand:brand Type:@"0"];
    
    [service requestDataWitnIndecator:^{
        
        [hudView hide:YES];
        
        if(service.status)
        {
            if(completion)
            {
                if(countPromotion==1){
                    
                    listPromotion = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                    
                }else{
                    
                    @try{
                        
                        NSArray *listPromotionNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        
                        if(listPromotionNew.count>0){
                            
                            [listPromotion addObjectsFromArray:listPromotionNew];
                            
                        }else{
                            
                            countPromotion--;
                        }
                    }
                    
                    @catch (NSException *exception) {
                        
                        countPromotion--;
                    }
                }
                
                completion();
            }
        }
        else
        {
            countPromotion--;
            
            completion();
            
        }
        
    }];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    countPromotion = 0;
    
    slug = @"";
    itemID = @"";
    
    userDefaults = [[NSUserDefaults alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadCoupon)
                                                 name:@"reloadCoupon"
                                               object:nil];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    isPromotion = YES;
    
    isLoader = YES;
    
    [self reqBrands:^{
        
        if(isPromotion) {
            
            [self reqPromotions:@"" :@"16" :slug :^{
                
                isLoader = NO;
                
                NSString *showHowtoPromotion = [NSString stringWithFormat:@"%@",  [userDefaults objectForKey:@"showHowtoPromotion"]];
                
                if([showHowtoPromotion isEqualToString:@"1"]) {
                    
                    [self.buttonHowto setHidden:NO];
                    
                }
                
                self.warnLabel.hidden = YES;
                
                self.tableOther.hidden = YES;
                
                if(listPromotion.count > 0) {
                    
                    self.tablePromotion.hidden = NO;
                    
                    [self.tablePromotion reloadData];
                }
            }];
            
        }else {
            
            [self reqLuckys:itemID :^(BOOL isSuscess) {
                
                self.tablePromotion.hidden = YES;
                
                if(listLucky.count > 0) {
                    
                    self.warnLabel.hidden = YES;
                    
                    self.tableOther.hidden = NO;
                    
                    [self.tableOther reloadData];
                    
                }else {
                    
                    [self reqMemberByEmail:^(BOOL isSuscess) {
                       
                        if(isSuscess) {
                            
                            self.warnLabel.hidden = NO;
                            
                            
                            self.tableOther.hidden = YES;
                        }
                    }];
                }
            }];
        }
    }];
}

- (void)reloadCoupon {
    
    [self reqLucky:itemID :^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (isPromotion)? [listPromotion count] : [listLucky count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    
    NSDictionary *dicPromotion;
    
    if(isPromotion) {
        dicPromotion = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPromotion objectAtIndex:indexPath.row]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicPromotion objectForKey:@"date"]];
    }else {
        dicPromotion = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
        BOOL can_use = [[dicPromotion objectForKey:@"can_use"] boolValue];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicPromotion objectForKey:@"message"]];
        if(can_use) {
            cell.detailTextLabel.textColor = color_green;
        }else {
            cell.detailTextLabel.textColor = [UIColor redColor];
        }
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [dicPromotion objectForKey:@"title"]];
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [dicPromotion objectForKey:@"thumbnail"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if(![thumbnail isEqualToString:@""])
    {
        [cell.imageView  sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:@"bjc_default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            UIImage *myIcon = [self imageByCroppingImage:image toSize:CGSizeMake(100, 100)];
            [cell.imageView setImage:myIcon];
        }];
    }else {
        UIImage *myIcon = [self imageByCroppingImage:[UIImage imageNamed:@"bjc_default.jpg"] toSize:CGSizeMake(100, 100)];
        [cell.imageView setImage:myIcon];
    }
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
 
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(isPromotion) {
        PromotionDescription *promotionDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionDescription"];
        promotionDescriptionVC.dicPromotion = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listPromotion objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:promotionDescriptionVC animated:YES];
    }else {
//        promotionDescriptionVC.dicPromotion = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listNotification objectAtIndex:indexPath.row]];
        NSDictionary *dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
        NSLog(@"%@", dicLucky);
        
        LuckyCampaign *luckyCampaignVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCampaign"];
        LuckyCoupon *luckyCouponVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCoupon"];
        
        if([[NSString stringWithFormat:@"%@",[dicLucky objectForKey:@"type"]]isEqualToString:@"campaign"]){
            luckyCampaignVC.dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:luckyCampaignVC animated:YES];
        }else if([[NSString stringWithFormat:@"%@",[dicLucky objectForKey:@"type"]]isEqualToString:@"coupon"]){
            luckyCouponVC.dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:luckyCouponVC animated:YES];
            
        }

    }
}

- (void)reqBrands:(void (^)(void))completion {
    
    RequestModel *service = [[RequestModel alloc]initGetBrands];
    
    [service requestDataWitnIndecator:^{
        
        listbrand = [[NSMutableArray alloc]init];
        
        if(service.status)
        {
            listbrand = [[NSMutableArray alloc]initWithArray:(NSArray*)[service.jsonData objectForKey:@"rows"]];
        }
        
        NSDictionary *objectDefaults = [[NSDictionary alloc]initWithObjectsAndKeys:@"0", @"id", @"ทั้งหมด", @"name", @"", @"slug", nil];
        
        [listbrand insertObject:objectDefaults atIndex:0];
        
        [_pickerView reloadAllComponents];
        
        completion();
        
    }];
}


- (void)reqPromotion: (NSString *)keyword :(NSString *)per_page :(NSString *)brand :(void (^)(void))completion {
    
    [hudView show:YES];
    countPromotion++;
    RequestModel *service = [[RequestModel alloc]initGetPromotionsWithKeyword:@"" PerPage:per_page Page:[NSString stringWithFormat:@"%d", countPromotion] Brand:brand Type:@"0"];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
           if(completion)
           {
               if(countPromotion==1){
                   listPromotion = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
               }else{
                   @try{
                       NSArray *listPromotionNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                       if(listPromotionNew.count>0){
                           [listPromotion addObjectsFromArray:listPromotionNew];
                       }else{
                           countPromotion--;
                       }
                   }
                   @catch (NSException *exception) {
                       countPromotion--;
                   }
               }
               
               _tablePromotion.hidden = (listPromotion.count == 0);
               
               [_tablePromotion reloadData];
               completion();
           }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

//- (void)reqNofication: (NSString *)keyword :(NSString *)per_page :(NSString *)brand :(void (^)(void))completion {
//    
//    [hudView show:YES];
//    countNotification++;
//    RequestModel *service = [[RequestModel alloc]initGetPromotionsWithKeyword:@"" PerPage:per_page Page:[NSString stringWithFormat:@"%d", countNotification] Brand:brand Type:@"1"];
//    [service requestDataWitnIndecator:^{[hudView hide:YES];
//        if(service.status)
//        {
//            if(completion)
//            {
//                if(countNotification==1){
//                    listNotification = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
//                }else{
//                    @try{
//                        NSArray *listPromotionNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
//                        if(listPromotionNew.count>0){
//                            [listNotification addObjectsFromArray:listPromotionNew];
//                        }else{
//                            countNotification--;
//                        }
//                    }
//                    @catch (NSException *exception) {
//                        countNotification--;
//                    }
//                }
//                [_tableOther reloadData];
//                completion();
//            }
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
//        
//    }];
//}

- (void)reqLucky :(NSString *)brand : (void (^)(void))completion {
    
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initGetLuckyDrawWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]] PerPage:@"-1" Page:@"1" Type:@"coupon" Brand:brand];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        isLoader = NO;
        if(service.status)
        {
            listLucky = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
            
            if(listLucky.count == 0) {
                
                [hudView show:YES];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                
                RequestModel *serviceGetMemberByEmail = [[RequestModel alloc]initGetMemberByEmailWithUsername:[NSString stringWithFormat:@"%@", [appDelegate.dicProfile objectForKey:@"user"]]];
                
                [serviceGetMemberByEmail requestDataWitnIndecator:^{
                    
                    [hudView hide:YES];
                    
                    if(serviceGetMemberByEmail.status)
                    {
                        NSDictionary *objectResult = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)serviceGetMemberByEmail.jsonData];
                        
                        BOOL isData = [[objectResult objectForKey:@"data"] boolValue];
                        
                        if(isData) {
                            
                            self.warnLabel.text = @"ระบบได้ทำการแจกคูปองเรียบร้อยแล้ว โปรดรอรับคูปองส่วนลดในรอบถัดไป";
                            
                        }else {
                            
                            self.warnLabel.text = @"ต้องการรับคูปองส่วนลด กรุณากรอกข้อมูลลงทะเบียนของท่านให้ครบถ้วน";
                        }
                        
                        
                        self.warnLabel.hidden = NO;
                        
                        self.tableOther.hidden = YES;
                        
                        self.tablePromotion.hidden = YES;
                        
                        completion();
                        
                    }else{
                        
                        completion();
                        
                        self.tablePromotion.hidden = YES;
                        
                        self.tableOther.hidden = YES;
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:serviceGetMemberByEmail.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                }];
                
            }
            else {
                
                completion();
                
                self.warnLabel.hidden = YES;
                
                self.tablePromotion.hidden = YES;
                
                self.tableOther.hidden = NO;
                
                [self.tableOther reloadData];
            }
        }
        else
        {
            
            completion();
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            
            if(isPromotion) {
                
                [self reqPromotions:@"" :@"16": slug :^{
                    
                    isLoader = NO;
                    
                    [self.tablePromotion reloadData];
                }];

            }else {
                
                [self reqLuckys:itemID :^(BOOL isSuscess) {
                    
                    isLoader = NO;
                    
                    [self.tableOther reloadData];
                }];
            }
        }
    }
}

- (IBAction)promotionClicked:(id)sender {
    
    if(!isPromotion) {
        
        [self.buttonNotification setBackgroundColor:color_gray];
        
        [self.buttonPromotion setBackgroundColor:[UIColor whiteColor]];
        
        isPromotion = YES;
        
        isLoader = YES;
        
        [hudView show:YES];
        
        
        countPromotion = 0;
        
        [self reqPromotions:@"" :@"16" :slug :^{
            
            isLoader = NO;
            
            self.warnLabel.hidden = YES;
            
            self.tableOther.hidden = YES;
            
            self.tablePromotion.hidden = NO;
            
            [self.tablePromotion reloadData];
            
            [hudView hide:YES];
        }];

        
    }
}

- (IBAction)notificationClicked:(id)sender {
    
    if(isPromotion) {
        
        [self.buttonNotification setBackgroundColor:[UIColor whiteColor]];
        
        [self.buttonPromotion setBackgroundColor:color_gray];
        
        isPromotion = NO;
        
        isLoader = YES;
        
        [hudView show:YES];
        
            [self reqLuckys:itemID :^(BOOL isSuscess) {
                
                self.tablePromotion.hidden = YES;
                
                if(listLucky.count > 0) {
                    
                    self.warnLabel.hidden = YES;
                    
                    self.tableOther.hidden = NO;
                    
                    [self.tableOther reloadData];
                    
                    [hudView hide:YES];
                    
                }else {
                    
                    [self reqMemberByEmail:^(BOOL isSuscess) {
                        
                        if(isSuscess) {
                            
                            [hudView hide:YES];
                            
                            self.warnLabel.hidden = NO;
                            
                            self.tableOther.hidden = YES;
                        }
                    }];
                    
                }
            }];

    }
}

- (IBAction)searchClicked:(id)sender {
    
    if([namePromtoin isEqualToString:@""] || namePromtoin==nil)
    {
        NSDictionary *dicBrand_ = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listbrand objectAtIndex:0]];
        slug = [NSString stringWithFormat:@"%@", [dicBrand_ objectForKey:@"slug"]];
        itemID = [NSString stringWithFormat:@"%@", [dicBrand_ objectForKey:@"id"]];
        namePromtoin = [NSString stringWithFormat:@"%@", [dicBrand_ objectForKey:@"name"]];
    }
    
    [self showPickerView];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)marketClicked:(id)sender {
    Market *marketVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Market"];
    [self.navigationController pushViewController:marketVC animated:YES];
}

- (IBAction)returnClicked:(id)sender {
    
    [hudView show:YES];
    
    slug = [NSString stringWithFormat:@"%@", [dicBrand objectForKey:@"slug"]];
    itemID = [NSString stringWithFormat:@"%@", [dicBrand objectForKey:@"id"]];
    namePromtoin = [NSString stringWithFormat:@"%@", [dicBrand objectForKey:@"name"]];
    
    [self hidePickerView];
    [self.searchBar setText:namePromtoin];
    
    isLoader = YES;
    
    countPromotion = 0;
    if([[self.searchBar text] isEqualToString:@"ทั้งหมด"])
    {
        [self reqPromotions:@"" :@"16": slug :^{
            
            [self reqLuckys:@"" :^(BOOL isSuscess) {
                
                isLoader = NO;
                
                self.warnLabel.hidden = isPromotion;
                
                self.tableOther.hidden = isPromotion;
                
                self.tablePromotion.hidden = !isPromotion;
                
                [self.tablePromotion reloadData];
                
                [self.tableOther reloadData];
                
                [hudView hide:YES];
            }];
            
        }];
        
    }else{
        
        [self reqPromotions:[self.searchBar text] :@"16": slug :^{
            
            [self reqLucky:itemID :^{
                
                isLoader = NO;
                
                self.warnLabel.hidden = isPromotion;
                
                self.tableOther.hidden = !isPromotion;
                
                self.tablePromotion.hidden = !isPromotion;
                
                [self.tablePromotion reloadData];
                
                [self.tableOther reloadData];
                
                [hudView hide:YES];
            }];
        }];
    }
}

- (IBAction)cancelClicked:(id)sender {
    [self hidePickerView];
}

- (IBAction)howtoClicked:(id)sender {
    [self.buttonHowto setHidden:YES];
    [userDefaults setObject:@"0" forKey:@"showHowtoPromotion"];
    [userDefaults synchronize];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
    dicBrand = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listbrand objectAtIndex:row]];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [listbrand count];
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//
//      NSDictionary *dicBrand = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listbrand objectAtIndex:row]];
//    return [NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [dicBrand objectForKey:@"name"]]];
//}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    NSDictionary *dicBrand = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listbrand objectAtIndex:row]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 46)];
    [label setText:[NSString stringWithFormat:@"       %@", [dicBrand objectForKey:@"name"]]];
    label.textAlignment = NSTextAlignmentCenter; //Changed to NS as UI is deprecated.
    label.backgroundColor = [UIColor clearColor];
    [label setTextColor:[UIColor whiteColor]];
    
    return label;
}

- (void)showPickerView {
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.viewPicker setFrame: CGRectMake(0, [UIScreen mainScreen].bounds.size.height-self.viewPicker.frame.size.height, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)hidePickerView {
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.viewPicker setFrame: CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"โปรโมชั่น"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
}

@end

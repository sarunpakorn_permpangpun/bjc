//
//  Augur.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Augur : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *dateNowLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableAugur;

- (IBAction)backClicked:(id)sender;

@end

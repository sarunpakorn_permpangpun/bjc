//
//  LuckyCoupon.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LuckyCoupon : UIViewController <UIAlertViewDelegate, UIWebViewDelegate>

@property (strong, nonatomic) NSDictionary *dicLucky;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *serialCouponTextField;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UIWebView *conditionWebView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *dateTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *idMaketTextField;
@property (weak, nonatomic) IBOutlet UIView *viewInputCoupon;
@property (weak, nonatomic) IBOutlet UIButton *maketRelationButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageBg;

@property (weak, nonatomic) IBOutlet UIView *viewCoupon;

- (IBAction)maketRelationClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)okClicked:(id)sender;
- (IBAction)tapAction:(id)sender;

@end

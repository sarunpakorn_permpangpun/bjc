//
//  ChangePassword.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "ChangePassword.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Menu.h"

@interface ChangePassword ()
{
    NSUserDefaults *userDefaults;
    AppDelegate *appDelegate;
}
@end

@implementation ChangePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [[NSUserDefaults alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)homeClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveClicked:(id)sender {
    
    NSString *password = self.passwordOldTextField.text;
    NSString *passwordNew = self.passwordNewTextField.text;
    NSString *passwordNewRetype = self.passwordNewReTypeTextField.text;
    
    if(password.length>0 && passwordNew.length>0 && passwordNewRetype.length>0) {
        if([passwordNew isEqualToString:passwordNewRetype]) {
            MBProgressHUD *hudView = [[MBProgressHUD alloc] initWithView:self.view];
            hudView.labelText = @"Loading";
            [self.view addSubview:hudView];
            [hudView show:YES];;
            RequestModel *service = [[RequestModel alloc]initChangePasswordWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]] OldPassword:self.passwordOldTextField.text NewPassword:self.passwordNewTextField.text];
            [service requestDataWitnIndecator:^{[hudView show:NO]; [hudView removeFromSuperview];
                if(service.status)
                {
                    appDelegate.dicProfile = [[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)[service.jsonData objectForKey:@"data"]];
                    [userDefaults setObject:@"" forKey:@"username"];
                    [userDefaults setObject:@"" forKey:@"password"];
                    [userDefaults synchronize];
                    
                    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                
            }];
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"รหัสผ่านใหม่ไม่ถูกต้อง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"รหัสผ่านไม่ถูกต้อง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

//
//  Notification.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Notification.h"
#import "AppDelegate.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"

#import "PromotionDescription.h"
#import "NewsDescription.h"
#import "LuckyCoupon.h"

#import "Winner.h"

@interface Notification ()
{
    NSArray *listNotification;
    MBProgressHUD *hudView;
    NSUserDefaults *userDefaults;
}
@end

@implementation Notification

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initGetNotificationWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            listNotification = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
            [_tableNotification reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listNotification count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSDictionary *dicNotification = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listNotification objectAtIndex:indexPath.row]];
    NSString *cellValue = [NSString stringWithFormat:@"%@", [dicNotification objectForKey:@"title"]];
    cell.textLabel.text = cellValue;
    
    NSString *statusString = [NSString stringWithFormat:@"%@", [dicNotification objectForKey:@"status"]];
    if(![statusString isEqualToString:@"U"])
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setTextColor:[UIColor grayColor]];
        
    }else{
        [cell setBackgroundColor:[UIColor lightGrayColor]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dicNotification = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listNotification objectAtIndex:indexPath.row]];

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell)
    {
        NSString *statusString = [NSString stringWithFormat:@"%@", [dicNotification objectForKey:@"status"]];
        if([statusString isEqualToString:@"U"])
        {
            [self reqReadNotice:[NSString stringWithFormat:@"%@", [dicNotification objectForKey:@"id"]] :[userDefaults objectForKey:@"username"] :^{
                if([[dicNotification objectForKey:@"type"] isEqualToString:@"promotion"] || [[dicNotification objectForKey:@"type"] isEqualToString:@"campaign"]){
                    if([[dicNotification objectForKey:@"type"] isEqualToString:@"promotion"])
                    {
                        PromotionDescription *promotionDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionDescription"];
                        promotionDescriptionVC.dicPromotion = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                        [self.navigationController pushViewController:promotionDescriptionVC animated:YES];
                        
                    }else {
                        Winner *winnerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Winner"];
                        NSDictionary *dicModified = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                     [dicNotification objectForKey:@"title"], @"title",
                                                     [dicNotification objectForKey:@"content"], @"code",
                                                     nil];
                        winnerVC.dicLucky = [[NSDictionary alloc]initWithDictionary:dicModified];
                        winnerVC.isFromViewNotification = YES;
                        [self.navigationController pushViewController:winnerVC animated:YES];
                    }
                
                }else if([[dicNotification objectForKey:@"type"] isEqualToString:@"news"] ){
                    NewsDescription *newsDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDescription"];
                    newsDescriptionVC.dicNews = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                    [self.navigationController pushViewController:newsDescriptionVC animated:YES];
                    
                }else if([[dicNotification objectForKey:@"type"] isEqualToString:@"coupon"]) {
                    
                    LuckyCoupon *luckyCouponVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCoupon"];
                    luckyCouponVC.dicLucky = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                    [self.navigationController pushViewController:luckyCouponVC animated:YES];
                    
                }
            }];
            
        }else {
            
            if([[dicNotification objectForKey:@"type"] isEqualToString:@"promotion"] || [[dicNotification objectForKey:@"type"] isEqualToString:@"campaign"]){
                if([[dicNotification objectForKey:@"type"] isEqualToString:@"promotion"])
                {
                    PromotionDescription *promotionDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionDescription"];
                    promotionDescriptionVC.dicPromotion = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                    [self.navigationController pushViewController:promotionDescriptionVC animated:YES];
                    
                }else{
                    
                    Winner *winnerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Winner"];
                    NSDictionary *dicModified = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 [dicNotification objectForKey:@"title"], @"title",
                                                 [dicNotification objectForKey:@"content"], @"code",
                                                 nil];
                    winnerVC.dicLucky = [[NSDictionary alloc]initWithDictionary:dicModified];
                    winnerVC.isFromViewNotification = YES;
                    [self.navigationController pushViewController:winnerVC animated:YES];
                }
                
            }else if([[dicNotification objectForKey:@"type"] isEqualToString:@"news"] ){
                
                NewsDescription *newsDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDescription"];
                newsDescriptionVC.dicNews = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                [self.navigationController pushViewController:newsDescriptionVC animated:YES];
                
            }else if([[dicNotification objectForKey:@"type"] isEqualToString:@"coupon"]) {
                
                LuckyCoupon *luckyCouponVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCoupon"];
                luckyCouponVC.dicLucky = [[NSDictionary alloc]initWithDictionary:[dicNotification objectForKey:@"content"]];
                [self.navigationController pushViewController:luckyCouponVC animated:YES];
                
            }
        }

    }
}

- (void)reqReadNotice: (NSString *)notice_id :(NSString *)user :(void (^)(void))completion {
    
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initReadNoticeWithNoticeId:notice_id Username:user];
    [service requestDataWitnIndecator:^{[hudView hide:YES];

            if(completion)
            {
                NSInteger badgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
                if(badgeNumber>0) {
                    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeNumber - 1;
                }
                completion();
            }

        
    }];
}

@end

//
//  ForgetPassword.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "ForgetPassword.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"

@interface ForgetPassword ()

@end

@implementation ForgetPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
} 


- (IBAction)submitButton:(id)sender {
    [self reqForgetPassword:[_usernameTextField text]];
}

- (IBAction)clearButton:(id)sender {
    [_usernameTextField setText:@""];
}

- (void)gotoLogin{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)reqForgetPassword: (NSString *)user {
    MBProgressHUD *hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    [hudView show:YES];;
    RequestModel *service = [[RequestModel alloc]initForgetPasswordWithUsername:[_usernameTextField text]];
    [service requestDataWitnIndecator:^{ [hudView show:NO]; [hudView removeFromSuperview];
        if(service.status)
        {
            [_viewMessage setHidden:NO];
            [self.view addSubview:_viewMessage];
            
            NSUserDefaults *userDefaults = [[NSUserDefaults alloc]init];
            [userDefaults setObject:@"" forKey:@"username"];
            [userDefaults setObject:@"" forKey:@"password"];
            [userDefaults synchronize];
            
            [NSTimer scheduledTimerWithTimeInterval:1.5f
                                             target:self
                                           selector:@selector(gotoLogin)
                                           userInfo:nil
                                            repeats:NO];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];}

@end

//
//  Setting.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Setting.h"
#import "EditProfile.h"
#import "ChangePassword.h"
#import "Login.h"
#import "AppDelegate.h"

#import "RequestModel.h"
#import "MBProgressHUD.h"

@interface Setting ()
{
    NSArray *listMenuSetting;
    AppDelegate *appDelegate;
    NSString *is_get_notice;
    
    NSUserDefaults *userDefaults;
    MBProgressHUD *hudView;
}
@end

@implementation Setting

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    userDefaults = [[NSUserDefaults alloc]init];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    is_get_notice = [NSString stringWithFormat:@"%@", [appDelegate.dicProfile objectForKey:@"is_get_notice"]];
    listMenuSetting = [[NSArray alloc]initWithObjects:@"แก้ไขข้อมูลส่วนตัว", @"แก้ไขรหัสผ่าน", @"การแจ้งเตือน", @"ออกจากระบบ", nil];
    [_tableSetting reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listMenuSetting count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        if(indexPath.row==2){
            UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
            [switchview addTarget:self action:@selector(changeStatusNofitication:) forControlEvents:UIControlEventValueChanged];
            if([is_get_notice isEqualToString:@"1"]){
                [switchview setOn:YES];
            }else{
                [switchview setOn:NO];
            }
            cell.accessoryView = switchview;
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *cellValue = [NSString stringWithFormat:@"%@", [listMenuSetting objectAtIndex:indexPath.row]];
    
    cell.textLabel.text = cellValue;
    return cell;
}

- (void) changeStatusNofitication:(UISwitch *)sender {
    
    if(sender.on == YES){
        [self reqSetNotice:@"1"];
    }else{
        [self reqSetNotice:@"0"];
    }
}

- (void)reqSetNotice: (NSString *)value {
    
    [hudView show:YES];
    
    RequestModel *service = [[RequestModel alloc]initSetGetNoticeWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]] Value:value];
    [service requestDataWitnIndecator:^{
        [hudView hide:NO];
        if(service.status)
        {
            [appDelegate.dicProfile setObject:value forKey:@"is_get_notice"];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditProfile *editProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
    ChangePassword *changePasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:@"คุณต้องการออกจากระบบ หรือไม่" delegate:self cancelButtonTitle:@"ยกเลิก" otherButtonTitles:@"ตกลง", nil];
    [alert setDelegate:self];
    
    switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:editProfileVC animated:YES];
            break;
        case 1:
            [self.navigationController pushViewController:changePasswordVC animated:YES];
            break;
        case 3:
            [alert show];
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
 
    if (buttonIndex == 1)
    {
        [userDefaults removeObjectForKey:@"username"];
        [userDefaults removeObjectForKey:@"password"];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        
    }
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
//
//  Login.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/29/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Login : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewLogin;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIView *viewPattan;

- (IBAction)loginSkipClicked:(id)sender;

- (IBAction)loginButton:(id)sender;
- (IBAction)registerButton:(id)sender;
- (IBAction)fbLoginButton:(id)sender;
- (IBAction)forgetButton:(id)sender;
@end

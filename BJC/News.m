//
//  News.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "News.h"
#import "AppDelegate.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"
#import "NewsDescription.h"
#import "UIImageView+WebCache.h"

#import <Google/Analytics.h>

@interface News ()
{
    NSMutableArray *listNews;
    int count;
    
    MBProgressHUD *hudView;
    BOOL isLoader;
}
@end

@implementation News

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 0;
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    isLoader = YES;
    [self reqNews:@"" :@"16":^{
        isLoader = NO;
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listNews count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    NSDictionary *dicNews = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listNews objectAtIndex:indexPath.row]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"title"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"date"]];
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"thumbnail"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if(![thumbnail isEqualToString:@""])
    {
        [cell.imageView  sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:@"bjc_default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            UIImage *myIcon = [self imageByCroppingImage:image toSize:CGSizeMake(100, 100)];
            [cell.imageView setImage:myIcon];
        }];
    }else {
        UIImage *myIcon = [self imageByCroppingImage:[UIImage imageNamed:@"bjc_default.jpg"] toSize:CGSizeMake(100, 100)];
        [cell.imageView setImage:myIcon];
    }
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsDescription *newsDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDescription"];
    newsDescriptionVC.dicNews = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listNews objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:newsDescriptionVC animated:YES];
}

- (void)reqNews: (NSString *)keyword :(NSString *)per_page :(void (^)(void))completion {

    [hudView show:YES];
    count++;
    RequestModel *service = [[RequestModel alloc]initGetNewsWithKeyword:keyword PerPage:per_page Page:[NSString stringWithFormat:@"%d", count]];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    listNews = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                }else{
                    @try{
                        NSArray *listPromotionNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        if(listPromotionNew.count>0){
                            [listNews addObjectsFromArray:listPromotionNew];
                        }else{
                            count--;
                        }
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableNews reloadData];
                completion();
            }

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}


- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okClicked:(id)sender {
    //    [self reqNews:[_searchTextField text] :@"16" :[NSString stringWithFormat:@"%d", count]];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            [self reqNews:@"" :@"16" :^{
                isLoader = NO;
            }];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ข่าวสาร"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end

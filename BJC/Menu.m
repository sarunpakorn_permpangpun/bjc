//
//  Menu.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Menu.h"
#import "Information.h"
#import "Notification.h"
#import "Setting.h"
#import "AppDelegate.h"

#import "EditProfile.h"

#import "Lucky.h"
#import "Promotion.h"
#import "News.h"
#import "Augur.h"
#import "Entertainment.h"
#import "InformationUpdate.h"

#import "RequestModel.h"
#import "MBProgressHUD.h"

#import <Google/Analytics.h>

#import "PromotionDescription.h"
#import "NewsDescription.h"
#import "LuckyCampaign.h"

@interface Menu ()
{
    AppDelegate *appDelegate;
    NSUserDefaults *userDefaults;
    MBProgressHUD *hudView;
}
@end

@implementation Menu

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    _imageDisplayCartoon.animationImages = [NSArray arrayWithObjects:
                                            [UIImage imageNamed:@"ani1-1.png"],
                                            [UIImage imageNamed:@"ani1-2.png"],
                                            [UIImage imageNamed:@"ani1-3.png"],
                                            [UIImage imageNamed:@"ani1-4.png"],
                                            [UIImage imageNamed:@"ani1-5.png"],
                                            [UIImage imageNamed:@"ani1-6.png"],
                                            [UIImage imageNamed:@"ani2-1.png"],
                                            [UIImage imageNamed:@"ani2-2.png"],
                                            [UIImage imageNamed:@"ani2-3.png"],
                                            [UIImage imageNamed:@"ani2-4.png"],
                                            [UIImage imageNamed:@"ani2-5.png"],
                                            [UIImage imageNamed:@"ani2-6.png"],
                                            [UIImage imageNamed:@"ani3-1.png"],
                                            [UIImage imageNamed:@"ani3-2.png"],
                                            [UIImage imageNamed:@"ani3-3.png"],
                                            [UIImage imageNamed:@"ani3-4.png"],
                                            [UIImage imageNamed:@"ani3-5.png"],
                                            [UIImage imageNamed:@"ani3-6.png"],
                                            [UIImage imageNamed:@"ani3-7.png"],
                                            [UIImage imageNamed:@"ani3-8.png"],
                                            [UIImage imageNamed:@"ani4-1.png"],
                                            [UIImage imageNamed:@"ani4-2.png"],
                                            [UIImage imageNamed:@"ani4-3.png"],
                                            [UIImage imageNamed:@"ani4-4.png"],
                                            [UIImage imageNamed:@"ani4-5.png"],
                                            [UIImage imageNamed:@"ani4-6.png"],
                                            [UIImage imageNamed:@"ani3-1.png"],
                                            [UIImage imageNamed:@"ani3-2.png"],
                                            [UIImage imageNamed:@"ani3-3.png"],
                                            [UIImage imageNamed:@"ani3-4.png"],
                                            [UIImage imageNamed:@"ani3-5.png"],
                                            [UIImage imageNamed:@"ani3-6.png"],
                                            [UIImage imageNamed:@"ani3-7.png"],
                                            [UIImage imageNamed:@"ani3-8.png"],
                                            nil];
    _imageDisplayCartoon.animationDuration = 5.25;
    _imageDisplayCartoon.animationRepeatCount = 1;
    [_imageDisplayCartoon startAnimating];
//
    [NSTimer scheduledTimerWithTimeInterval:5.2f
                                     target:self
                                   selector:@selector(changeSpeedDisplayCartoon)
                                   userInfo:nil
                                    repeats:NO];

    userDefaults = [[NSUserDefaults alloc]init];
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    
    NSString *showHowtoMenu = [NSString stringWithFormat:@"%@",  [userDefaults objectForKey:@"showHowtoMenu"]];
    if([showHowtoMenu isEqualToString:@"1"]) {
        [self.buttonHowto setHidden:NO];
    }
    
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tranferID) name:@"tranferID" object:nil];
}

- (void) changeSpeedDisplayCartoon {
    _imageDisplayCartoon.animationImages = [NSArray arrayWithObjects:
                                            [UIImage imageNamed:@"ani5-1.png"],
                                            [UIImage imageNamed:@"ani5-2.png"],
                                            [UIImage imageNamed:@"ani5-3.png"],
                                            [UIImage imageNamed:@"ani5-4.png"],
                                            [UIImage imageNamed:@"ani5-5.png"],
                                            [UIImage imageNamed:@"ani5-6.png"],
                                            [UIImage imageNamed:@"ani5-7.png"],
                                            [UIImage imageNamed:@"ani5-1.png"],
                                            [UIImage imageNamed:@"ani5-2.png"],
                                            [UIImage imageNamed:@"ani5-3.png"],
                                            [UIImage imageNamed:@"ani5-4.png"],
                                            [UIImage imageNamed:@"ani5-5.png"],
                                            [UIImage imageNamed:@"ani5-6.png"],
                                            [UIImage imageNamed:@"ani5-7.png"],
                                            [UIImage imageNamed:@"ani5-1.png"],
                                            [UIImage imageNamed:@"ani5-2.png"],
                                            [UIImage imageNamed:@"ani5-3.png"],
                                            [UIImage imageNamed:@"ani5-4.png"],
                                            [UIImage imageNamed:@"ani5-5.png"],
                                            [UIImage imageNamed:@"ani5-6.png"],
                                            [UIImage imageNamed:@"ani5-7.png"],
                                            nil];
    _imageDisplayCartoon.animationDuration = 4.25f;
    _imageDisplayCartoon.animationRepeatCount = 0;
    [_imageDisplayCartoon startAnimating];
    
}

- (void)tranferID {
    if([appDelegate.pageid isEqualToString:@""] || !appDelegate.pageid) {
        
    }else {
        [hudView show:YES];
        RequestModel *service = [[RequestModel alloc]initGetById:appDelegate.pageid  Username:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]];
        [service requestDataWitnIndecator:^{[hudView hide:YES];
            if(service.status)
            {
                NSDictionary *dicResult = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)[service.jsonData objectForKey:@"rows"]];
                if([[dicResult objectForKey:@"type"] isEqualToString:@"campaign"]) {
                    
                     LuckyCampaign *luckyCampaignVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCampaign"];
                    luckyCampaignVC.dicLucky = [[NSDictionary alloc]initWithDictionary:dicResult];
                    [self.navigationController pushViewController:luckyCampaignVC animated:YES];
                    
                }else if([[dicResult objectForKey:@"type"] isEqualToString:@"coupon"]) {
                    
                    PromotionDescription *promotionDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionDescription"];
                    promotionDescriptionVC.dicPromotion = [[NSDictionary alloc]initWithDictionary:dicResult];
                    [self.navigationController pushViewController:promotionDescriptionVC animated:YES];
                    
                }else if([[dicResult objectForKey:@"type"] isEqualToString:@"news"]) {
                    
                    NewsDescription *newsDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDescription"];
                    newsDescriptionVC.dicNews = [[NSDictionary alloc]initWithDictionary:dicResult];
                    [self.navigationController pushViewController:newsDescriptionVC animated:YES];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
        }];
        
        appDelegate.pageid = @"";
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self tranferID];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Main Page"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initGetNotificationWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            NSMutableArray *listNotification = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
            if(listNotification.count>0)
            {
                BOOL isNotice = NO;
                for(NSDictionary *notificaionInfo in listNotification)
                {
                    NSString *statusString = [NSString stringWithFormat:@"%@", [notificaionInfo objectForKey:@"status"]];
                    if([statusString isEqualToString:@"U"])
                    {
                        isNotice = YES;
                        break;
                    }
                }
                
                [self.noticeImageView setHidden:!isNotice];
                
                
            }
            
            
            RequestModel *serviceCountNoti = [[RequestModel alloc]initGetCountNotificationWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]];
            [serviceCountNoti requestDataWitnIndecator:^{[hudView hide:YES];
                if(serviceCountNoti.status)
                {
                    NSString *countNotiBadge = [serviceCountNoti.jsonData objectForKey:@"count"];
                    
                    [UIApplication sharedApplication].applicationIconBadgeNumber = [countNotiBadge integerValue];
                }
            }];

        }
        else
        {
            [hudView hide:YES];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)informationClicked:(id)sender {
    Information *informationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Information"];
    [self.navigationController pushViewController:informationVC animated:YES];
}

- (IBAction)noticeClicked:(id)sender {
    Notification *notificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Notification"];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (IBAction)settingClicked:(id)sender {
    Setting *settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Setting"];
    [self.navigationController pushViewController:settingVC animated:YES];
}

- (IBAction)menuClicked:(id)sender {
    
    Lucky *luckyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Lucky"];
    Promotion *promotionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Promotion"];
    News *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"News"];
    Augur *augurVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Augur"];
    Entertainment *entertainmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Entertainment"];
    InformationUpdate *informationUpdateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InformationUpdate"];
    switch ([sender tag]) {
        case 0:
//            if([self reCheckProfile]){
                [self.navigationController pushViewController:luckyVC animated:YES];
//            }else{
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ข้อมูลไม่สมบรูณ์" message:@"ต้องการกรอกข้อมูลเพิ่มเติมหรือไม่"  delegate:self cancelButtonTitle:@"ยกเลิก" otherButtonTitles: @"ตกลง", nil];
//                [alert show];
//            }
            break;
        case 1:
            [self.navigationController pushViewController:promotionVC animated:YES];
            break;
        case 2:
            [self.navigationController pushViewController:newsVC animated:YES];
            break;
        case 3:
            [self.navigationController pushViewController:augurVC animated:YES];
            break;
        case 4:
            [self.navigationController pushViewController:entertainmentVC animated:YES];
            break;
        case 5:
            [self.navigationController pushViewController:informationUpdateVC animated:YES];
            break;
        default:
            break;
    }
    
}

- (IBAction)howtoClicked:(id)sender {
    [self.buttonHowto setHidden:YES];
    
    [userDefaults setObject:@"0" forKey:@"showHowtoMenu"];
    [userDefaults synchronize];
}
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1)
    {
        EditProfile *editProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
        [self.navigationController pushViewController:editProfileVC animated:YES];
    }
}
- (BOOL) reCheckProfile{
    NSString *name = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"name"]];
    NSString *surname = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"surname"]];
    NSString *address = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"address"]];
    NSString *road = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"road"]];
    NSString *sub_district = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"sub-district"]];
    NSString *district = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"district"]];
    NSString *province = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"province"]];
    NSString *zipcode = [NSString stringWithFormat:@"%@",[appDelegate.dicProfile objectForKey:@"zipcode"]];
    
    if([name length]==0|| [surname length]==0|| [address length]==0|| [road length]==0|| [sub_district length]==0|| [district length]==0|| [province length]==0|| [zipcode length]==0){
        return false;
    }else{
        return true;
    }
}



@end

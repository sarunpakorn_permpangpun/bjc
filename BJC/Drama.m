//
//  Drama.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Drama.h"
#import "AppDelegate.h"
#import "RequestModelADV.h"
#import "MBProgressHUD.h"
#import "DramaDescription.h"
#import "UIImageView+WebCache.h"

@interface Drama ()
{
    NSMutableArray *listDrama;
   
    int count;
    
    NSString *chanelTVString;
    BOOL isLoader;
    MBProgressHUD *hudView;
}
@end

@implementation Drama

- (void)viewDidLoad {
    [super viewDidLoad];
    
    count = 0;
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    [self reqSeriesChapters:@"" :@"16":^{
        [hudView hide:YES];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listDrama count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    
    NSDictionary *dicDrama = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listDrama objectAtIndex:indexPath.row]];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", self.nameDrama, [dicDrama objectForKey:@"chapter"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"date"]];
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"thumbnail"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if(![thumbnail isEqualToString:@""]) {
        
        [cell.imageView  sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:@"bjc_default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [cell.imageView setImage:image];
            [cell.imageView setContentMode:UIViewContentModeScaleAspectFit];
        }];
    }else {
        UIImage *myIcon = [self imageByCroppingImage:[UIImage imageNamed:@"bjc_default.jpg"] toSize:CGSizeMake(100, 100)];
        [cell.imageView setImage:myIcon];
    }
    
    return cell;
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DramaDescription *dramaDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DramaDescription"];
    dramaDescriptionVC.dicDrama = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listDrama objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:dramaDescriptionVC animated:YES];
}

- (void)reqSeriesChapters: (NSString *)keyword :(NSString *)per_page :(void (^)(void))completion {
    
    [hudView show:YES];
    count++;
    RequestModelADV *service = [[RequestModelADV alloc]initGetSeriesChaptersWithKeyword:keyword Series:self.slug PerPage:per_page Page:[NSString stringWithFormat:@"%d", count]];
    [service requestDataWitnIndecator:^{[hudView show:NO]; [hudView removeFromSuperview];
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    listDrama = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                }else{
                    @try{
                        NSArray *listDramaNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        if(listDramaNew.count>0){
                            [listDrama addObjectsFromArray:listDramaNew];
                        }else{
                            count--;
                        }
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableDramaSub reloadData];
                completion();
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            [self reqSeriesChapters:@"" :@"16" :^{
                isLoader = NO;
                [hudView hide:YES];
            }];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
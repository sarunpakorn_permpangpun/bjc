//
//  RequestModel.h
//  TarotCard
//
//  Created by Sarunpakorn Permpangpun on 2/12/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "BJCHTTPRequest.h"
#import <Foundation/Foundation.h>

@interface RequestModel : BJCHTTPRequest <BJCHTTPRequestDelegate>

@property (readonly, strong, nonatomic) NSDictionary *jsonData;

-(id)initGetMemberByEmailWithUsername:(NSString *)user;

-(id)initChangePasswordWithUsername:(NSString *)user OldPassword:(NSString *)oldpw NewPassword:(NSString *)newpw;
-(id)initEditProfileWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode;
-(id)initForgetPasswordWithUsername:(NSString *)user;
-(id)initGetBrands;
-(id)initGetCCAATT;
-(id)initGetEntertainmentsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetGoldPriceWithId:(NSString *)id_;
-(id)initGetHoroscopeWithDate:(NSString *)date;
-(id)initGetLotteryResultsWithId:(NSString *)id_;
-(id)initGetLuckyDrawWithUsername:(NSString *)user PerPage:(NSString *)per_page Page:(NSString *)page Type:(NSString *)type Brand:(NSString *)brand;
-(id)initGetNewsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetNotificationWithUsername:(NSString *)user;
-(id)initGetCountNotificationWithUsername:(NSString *)user;
-(id)initGetOilPriceWithId:(NSString *)id_;
-(id)initGetPromotionsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page Brand:(NSString *)brand Type:(NSString *)type;
-(id)initGetSeriesChaptersWithKeyword:(NSString *)keyword Series:(NSString *)series PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetSeriesNameWithChannel:(NSString *)channel PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetStoresWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initLogoutWithUsername:(NSString *)user;
-(id)initLoginWithUsername:(NSString *)user Password:(NSString *)pw RegisterId:(NSString *)regid Device:(NSString *)device;
-(id)initReadNoticeWithNoticeId:(NSString *)notice_id Username:(NSString *)user;
-(id)initRegisterWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode;
-(id)initSetGetNoticeWithUsername:(NSString *)user Value:(NSString *)value;
-(id)initUseCouponWithCouponId:(NSString *)coupon_id CouponCode:(NSString *)coupon_code StoreCode:(NSString *)store_code;
-(id)initGetCouponStore:(NSString*)coupon_id PerPage:(NSString*)per_page Page:(NSString*)page;
-(id)initGetById:(NSString*)id_ Username:(NSString *)user;

@end

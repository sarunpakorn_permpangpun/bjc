//
//  RequestModel.m
//  TarotCard
//
//  Created by Sarunpakorn Permpangpun on 2/12/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "RequestModel.h"

@interface RequestModel()
{
    NSMutableDictionary *postData;
    NSString *request_Path;
}
@end

@implementation RequestModel


-(id)initGetMemberByEmailWithUsername:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"checkmemberinfobyemail";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initChangePasswordWithUsername:(NSString *)user OldPassword:(NSString *)oldpw NewPassword:(NSString *)newpw
{
    self = [super init];
    if(self)
    {
        request_Path = @"changepw";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:oldpw forKey:@"oldpw"];
        [postData setObject:newpw forKey:@"newpw"];
    }
    return self;
}

-(id)initEditProfileWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode
{
    self = [super init];
    if(self)
    {
        request_Path = @"editprofile";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:pw forKey:@"pw"];
        [postData setObject:firstname forKey:@"firstname"];
        [postData setObject:surname forKey:@"surname"];
        [postData setObject:age forKey:@"age"];
        [postData setObject:gender forKey:@"gender"];
        [postData setObject:email forKey:@"email"];
        [postData setObject:address forKey:@"address"];
        [postData setObject:zipcode forKey:@"zipcode"];
        [postData setObject:tel forKey:@"tel"];
        [postData setObject:road forKey:@"road"];
        [postData setObject:subdistrict forKey:@"sub-district"];
        [postData setObject:district forKey:@"district"];
        [postData setObject:province forKey:@"province"];
        [postData setObject:zipcode forKey:@"zipcode"];
    }
    return self;
}

-(id)initForgetPasswordWithUsername:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"forgetpw";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initGetBrands
{
    self = [super init];
    if(self)
    {
        request_Path = @"getbrands";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id)initGetCCAATT
{
    self = [super init];
    if(self)
    {
        request_Path = @"getccaatt";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id)initGetEntertainmentsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getentertain";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:keyword forKey:@"keyword"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}

-(id)initGetGoldPriceWithId:(NSString *)id_
{
    self = [super init];
    if(self)
    {
        request_Path = @"getgold";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:id_ forKey:@"id"];
    }
    return self;
}

-(id)initGetHoroscopeWithDate:(NSString *)date
{
    self = [super init];
    if(self)
    {
        request_Path = @"gethoro";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:date forKey:@"date"];
    }
    return self;
}

-(id)initGetLotteryResultsWithId:(NSString *)id_
{
    self = [super init];
    if(self)
    {
        request_Path = @"getlottery";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:id_ forKey:@"id"];
    }
    return self;
}

-(id)initGetLuckyDrawWithUsername:(NSString *)user PerPage:(NSString *)per_page Page:(NSString *)page Type:(NSString *)type Brand:(NSString *)brand
{
    self = [super init];
    if(self)
    {
        request_Path = @"getlucky";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
        [postData setObject:type forKey:@"type"];
        [postData setObject:brand forKey:@"brand"];
    }
    return self;
}

-(id)initGetNewsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getnews";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:keyword forKey:@"keyword"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}


-(id)initGetCountNotificationWithUsername:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"getnoticecount";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initGetNotificationWithUsername:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"getnotice";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initGetOilPriceWithId:(NSString *)id_
{
    self = [super init];
    if(self)
    {
        request_Path = @"getoil";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:id_ forKey:@"id"];
    }
    return self;
}

-(id)initGetPromotionsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page Brand:(NSString *)brand Type:(NSString *)type
{
    self = [super init];
    if(self)
    {
        request_Path = @"getpromotion";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:keyword forKey:@"keyword"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
        [postData setObject:brand forKey:@"brand"];
        [postData setObject:type forKey:@"type"];
    }
    return self;
}

-(id)initGetSeriesChaptersWithKeyword:(NSString *)keyword Series:(NSString *)series PerPage:(NSString *)per_page Page:(NSString *)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getchapter";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:keyword forKey:@"keyword"];
        [postData setObject:series forKey:@"series"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}

-(id)initGetSeriesNameWithChannel:(NSString *)channel PerPage:(NSString *)per_page Page:(NSString *)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getseries";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:channel forKey:@"channel"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}

-(id)initGetStoresWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getstores";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:keyword forKey:@"keyword"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}

-(id)initLogoutWithUsername:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"logout";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initLoginWithUsername:(NSString *)user Password:(NSString *)pw RegisterId:(NSString *)regid Device:(NSString *)device
{
    self = [super init];
    if(self)
    {
        request_Path = @"login";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:pw forKey:@"pw"];
        [postData setObject:regid forKey:@"regid"];
        [postData setObject:device forKey:@"device"];
    }
    return self;
}

-(id)initReadNoticeWithNoticeId:(NSString *)notice_id Username:(NSString *)user
{
    self = [super init];
    if(self)
    {
        request_Path = @"readnotice";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:notice_id forKey:@"notice_id"];
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(id)initRegisterWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode
{
    self = [super init];
    if(self)
    {
        request_Path = @"register";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:pw forKey:@"pw"];
        [postData setObject:firstname forKey:@"firstname"];
        [postData setObject:surname forKey:@"surname"];
        [postData setObject:age forKey:@"age"];
        [postData setObject:gender forKey:@"gender"];
        [postData setObject:email forKey:@"email"];
        [postData setObject:address forKey:@"address"];
        [postData setObject:road forKey:@"road"];
        [postData setObject:subdistrict forKey:@"sub-district"];
        [postData setObject:district forKey:@"district"];
        [postData setObject:province forKey:@"province"];
        [postData setObject:zipcode forKey:@"zipcode"];
        [postData setObject:tel forKey:@"tel"];
    }
    return self;
}

-(id)initSetGetNoticeWithUsername:(NSString *)user Value:(NSString *)value
{
    self = [super init];
    if(self)
    {
        request_Path = @"setgetnotice";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:user forKey:@"user"];
        [postData setObject:value forKey:@"value"];
    }
    return self;
}

-(id)initUseCouponWithCouponId:(NSString *)coupon_id CouponCode:(NSString *)coupon_code StoreCode:(NSString *)store_code
{
    self = [super init];
    if(self)
    {
        request_Path = @"usecoupon";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:coupon_id forKey:@"coupon_id"];
        [postData setObject:coupon_code forKey:@"coupon_code"];
        [postData setObject:store_code forKey:@"store_code"];
    }
    return self;
}

-(id)initGetCouponStore:(NSString*)coupon_id PerPage:(NSString*)per_page Page:(NSString*)page
{
    self = [super init];
    if(self)
    {
        request_Path = @"getcouponstore";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:coupon_id forKey:@"coupon_id"];
        [postData setObject:per_page forKey:@"per_page"];
        [postData setObject:page forKey:@"page"];
    }
    return self;
}

-(id)initGetById:(NSString *)id_ Username:(NSString *)user {
    self = [super init];
    if(self)
    {
        request_Path = @"getbyid";
        delegate = self;
        postData = [[NSMutableDictionary alloc] init];
        
        [postData setObject:id_ forKey:@"id"];
        [postData setObject:user forKey:@"user"];
    }
    return self;
}

-(NSString *)requestUrlString
{
    return request_Path;
}

-(NSMutableDictionary *)postData
{
    return postData;
}

-(void)httpRequest:(BJCHTTPRequest *)request didSuccessWithData:(NSDictionary *)data
{
    _jsonData = data;
}

-(void)httpRequest:(BJCHTTPRequest *)request didFailWithErrorMessage:(NSString *)errorMsg
{
    NSLog(@"Error:%@", errorMsg);
}
@end

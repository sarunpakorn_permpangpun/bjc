//
//  RequestModelADV.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "ADVHTTPRequest.h"
#import <Foundation/Foundation.h>

@interface RequestModelADV : ADVHTTPRequest <ADVHTTPRequestDelegate>

@property (readonly, strong, nonatomic) NSDictionary *jsonData;

-(id)initChangePasswordWithUsername:(NSString *)user OldPassword:(NSString *)oldpw NewPassword:(NSString *)newpw;
-(id)initEditProfileWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode;
-(id)initForgetPasswordWithUsername:(NSString *)user;
-(id)initGetBrands;
-(id)initGetCCAATT;
-(id)initGetEntertainmentsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetGoldPrice;
-(id)initGetHoroscopeWithDate:(NSString *)date;
-(id)initGetLotteryResults;
-(id)initGetLuckyDrawWithUsername:(NSString *)user;
-(id)initGetNewsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetNotificationWithUsername:(NSString *)user;
-(id)initGetOilPrice;
-(id)initGetPromotionsWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page Brand:(NSString *)brand;
-(id)initGetSeriesChaptersWithKeyword:(NSString *)keyword Series:(NSString *)series PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetSeriesNameWithChannel:(NSString *)channel PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initGetStoresWithKeyword:(NSString *)keyword PerPage:(NSString *)per_page Page:(NSString *)page;
-(id)initLogoutWithUsername:(NSString *)user;
-(id)initLoginWithUsername:(NSString *)user Password:(NSString *)pw RegisterId:(NSString *)regid Device:(NSString *)device;
-(id)initReadNoticeWithNoticeId:(NSString *)notice_id Username:(NSString *)user;
-(id)initRegisterWithUsername:(NSString *)user Password:(NSString *)pw Firstname:(NSString *)firstname Surname:(NSString *)surname Age:(NSString *)age Gender:(NSString *)gender Email:(NSString *)email Tel:(NSString *)tel Address:(NSString *)address Road:(NSString *)road Subdistrict:(NSString *)subdistrict District:(NSString *)district Province:(NSString *)province Zipcode:(NSString *)zipcode;
-(id)initSetGetNoticeWithUsername:(NSString *)user Value:(NSString *)value;
-(id)initUseCouponWithCouponId:(NSString *)coupon_id CouponCode:(NSString *)coupon_code StoreCode:(NSString *)store_code;
-(id)initGetChannel;

@end

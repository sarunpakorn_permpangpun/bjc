//
//  ADVHTTPRequest.h
//  TarotCard
//
//  Created by Sarunpakorn Permpangpun on 2/12/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADVHTTPRequest;

@protocol ADVHTTPRequestDelegate <NSObject>

@required
-(NSString *)requestUrlString;

@optional
-(NSDictionary *)dummyData;
-(NSMutableDictionary *)postData;

-(void)httpRequest:(ADVHTTPRequest *)request didSuccessWithData:(NSDictionary *)data;
-(void)httpRequest:(ADVHTTPRequest *)request didFailWithErrorMessage:(NSString *)errorMsg;

@end

@interface ADVHTTPRequest : NSObject
{
    id<ADVHTTPRequestDelegate> delegate;
}

@property (readonly, assign) BOOL status;
@property (readonly, strong, nonatomic) NSString *errorMessage;
@property (readonly, strong, nonatomic) NSString *successDescription;

-(void)requestData:(void (^)(void))completion;
-(void)requestDataWitnIndecator:(void (^)(void))completion;
//-(void)requestDataWitnIndecatorinView:(UIView *)view Completion:(void (^)(void))completion;

@end
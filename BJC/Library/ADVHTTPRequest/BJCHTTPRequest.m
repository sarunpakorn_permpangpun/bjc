//
//  ADVHTTPRequest.m
//  TarotCard
//
//  Created by Sarunpakorn Permpangpun on 2/12/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "BJCHTTPRequest.h"
#import "AFNetworking.h"
//#import "MBProgressHUD.h"

#define ENDPOINT_URL @"http://58.137.160.85/bjc/api-get-lists/?action="

//#define ENDPOINT_URL @"http://chat.shinee.com/bjcadmin-demo/api-get-lists/?action="

@interface BJCHTTPRequest()
{
    NSMutableDictionary *postData;
}

@end

@implementation BJCHTTPRequest

-(id)init
{
    self = [super init];
    if(self)
    {
        postData = [[NSMutableDictionary alloc] init];
        _status = NO;
    }
    return self;
}

//-(void)requestDataWitnIndecatorinView:(UIView *)view Completion:(void (^)(void))completion
//{
//    MBProgressHUD *hudView;
//    
//    if(view)
//    {
//        hudView = [[MBProgressHUD alloc] initWithView:view];
//        hudView.labelText = @"Loading";
//        [view addSubview:hudView];
//        [hudView show:YES];
//    }
//    
//    [self requestData:^{
//        
//        if(view)
//        {
//            [hudView show:NO];
//            [hudView removeFromSuperview];
//        }
//        
//        if(completion)
//        {
//            completion();
//        }
//    }];
//}

-(void)requestDataWitnIndecator:(void (^)(void))completion{
    [self requestData:^{
        
        if(completion)
        {
            completion();
        }
    }];

}

-(void)requestData:(void (^)(void))completion
{
    if([delegate respondsToSelector:@selector(postData)])
    {
        NSString *requestPath = nil;
        
        if([delegate respondsToSelector:@selector(requestUrlString)])
        {
            requestPath = [delegate requestUrlString];
        }
        
        if(requestPath)
        {
            NSDictionary *dummyData = nil;
            if([delegate respondsToSelector:@selector(dummyData)])
            {
                dummyData = [delegate dummyData];
            }
            
            if(dummyData)
            {
                dispatch_queue_t dummyQueue = dispatch_queue_create("dummy", NULL);
                dispatch_async(dummyQueue, ^{
                    
                    sleep(1);
                    
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        
                        if([dummyData isKindOfClass:[NSDictionary class]])
                        {
                            NSString *resultKey = @"status";
                            
                            if([[NSString stringWithFormat:@"%@",[dummyData objectForKey:resultKey]]isEqualToString:@"ok"])
                            {
                                
                                _status = YES;
                                _successDescription = [NSString stringWithFormat:@"%@",[dummyData objectForKey:@"message"]];
                                
                                if([delegate respondsToSelector:@selector(httpRequest:didSuccessWithData:)])
                                {
                                    [delegate httpRequest:self didSuccessWithData:dummyData];
                                }
                                
                                completion();
                            }
                            else
                            {
                                NSLog(@"Response Error %@",dummyData);
                                
                                _errorMessage = [NSString stringWithFormat:@"%@",[dummyData objectForKey:@"message"]];
                                
                                if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
                                {
                                    [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                                }
                                
                                completion();
                            }
                        }
                        else
                        {
                            
                            _errorMessage = @"Response not JSON Data";
                            
                            if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
                            {
                                [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                            }
                            
                            completion();
                        }
                        
                    });
                });
                
                //                dispatch_release(dummyQueue);
                
            }
            else
            {
                NSString *requestURL = @"";
                
                if([requestPath hasPrefix:@"http"])
                {
                    requestURL = requestPath;
                }
                else
                {
                    requestURL = [NSString stringWithFormat:@"%@%@",ENDPOINT_URL,requestPath];
                }
                
                NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:requestURL parameters:[delegate postData] error:nil];
                
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                operation.securityPolicy.allowInvalidCertificates = YES;
                operation.responseSerializer = [AFJSONResponseSerializer serializer];
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject){
                    
                    
                    if([responseObject isKindOfClass:[NSDictionary class]])
                    {
                        NSString *resultKey = @"status";
                        
                        if([[NSString stringWithFormat:@"%@",[responseObject objectForKey:resultKey]]isEqualToString:@"ok"])
                        {
                            
                            _status = YES;
                            _successDescription = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"message"]];
                            
                            if([delegate respondsToSelector:@selector(httpRequest:didSuccessWithData:)])
                            {
                                [delegate httpRequest:self didSuccessWithData:responseObject];
                            }
                            
                            completion();
                        }
                        else
                        {
                            NSLog(@"Response Error %@",responseObject);
                            
                            _errorMessage = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"message"]];
                            
                            if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
                            {
                                [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                            }
                            
                            completion();
                        }
                    }
                    else
                    {
                        
                        _errorMessage = @"Response not JSON Data";
                        
                        if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
                        {
                            [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                        }
                        
                        completion();
                    }
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error){
                    
                    _errorMessage = [error  localizedDescription];
                    
                    if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
                    {
                        [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                    }
                    
                    completion();
                    
                }];
                
                [operation start];
                
            }
        }
        else
        {
            
            _errorMessage = @"No URL";
            
            if([delegate respondsToSelector:@selector(httpRequest:didFailWithErrorMessage:)])
            {
                [delegate httpRequest:self didFailWithErrorMessage:_errorMessage];
                
            }
            
            completion();
        }
    }
}

@end
//
//  ADVHTTPRequest.h
//  TarotCard
//
//  Created by Sarunpakorn Permpangpun on 2/12/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BJCHTTPRequest;

@protocol BJCHTTPRequestDelegate <NSObject>

@required
-(NSString *)requestUrlString;

@optional
-(NSDictionary *)dummyData;
-(NSMutableDictionary *)postData;

-(void)httpRequest:(BJCHTTPRequest *)request didSuccessWithData:(NSDictionary *)data;
-(void)httpRequest:(BJCHTTPRequest *)request didFailWithErrorMessage:(NSString *)errorMsg;

@end

@interface BJCHTTPRequest : NSObject
{
    id<BJCHTTPRequestDelegate> delegate;
}

@property (readonly, assign) BOOL status;
@property (readonly, strong, nonatomic) NSString *errorMessage;
@property (readonly, strong, nonatomic) NSString *successDescription;

-(void)requestData:(void (^)(void))completion;
-(void)requestDataWitnIndecator:(void (^)(void))completion;
//-(void)requestDataWitnIndecatorinView:(UIView *)view Completion:(void (^)(void))completion;

@end
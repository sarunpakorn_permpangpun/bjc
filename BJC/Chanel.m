//
//  Chanel.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Chanel.h"
#import "AppDelegate.h"
#import "Drama.h"
#import "RequestModelADV.h"
#import "MBProgressHUD.h"

@interface Chanel ()
{
    NSMutableArray *listSeries;
    
    int count;
    
    NSString *chanelTVString;
    BOOL isLoader;
    MBProgressHUD *hudView;
}
@end

@implementation Chanel

- (void)viewDidLoad {
    [super viewDidLoad];
//    if([_tvChanel isEqualToString:@"ch3"]){
//        [_tvChanelLabel setText:@"ช่อง 3"];
//    }else if([_tvChanel isEqualToString:@"ch5"]){
//        [_tvChanelLabel setText:@"ช่อง 5"];
//    }else if([_tvChanel isEqualToString:@"ch7"]){
    
        [_tvChanelLabel setText:[NSString stringWithFormat:@"ช่อง %@", [self.tvChanel substringFromIndex:2]]];
//    }
    
    count = 0;
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    [self reqSeriesName:@"16" :^{
        [hudView hide:YES];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listSeries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSDictionary *dicSeries = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listSeries objectAtIndex:indexPath.row]];
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dicSeries objectForKey:@"thumbnail"]]];
    UIImage *myIcon = [self imageWithImage:[UIImage imageWithData:data] scaledToSize:CGSizeMake(100, 100)];
    [cell.imageView setImage:myIcon];
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@", [dicSeries objectForKey:@"name"]]];
    [cell.textLabel setTextColor:color_green];
    
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)reqSeriesName:(NSString *)per_page :(void (^)(void))completion {

    [hudView show:YES];
    count++;
    RequestModelADV *service = [[RequestModelADV alloc]initGetSeriesNameWithChannel:self.tvChanel PerPage:per_page Page:[NSString stringWithFormat:@"%d", count]];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    listSeries = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                }else{
                    @try{
                        NSArray *listSeriesNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        if(listSeriesNew.count>0){
                            [listSeries addObjectsFromArray:listSeriesNew];
                        }else{
                            count--;
                        }
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableChanel reloadData];
                completion();
            }
        }
        else
        {
            [hudView hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
            
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            [self reqSeriesName:@"16":^{
                isLoader = NO;
                [hudView hide:YES];
            }];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Drama *dramaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Drama"];
    NSDictionary *dicDrama = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listSeries objectAtIndex:indexPath.row]];
    dramaVC.slug = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"slug"]];
    dramaVC.nameDrama = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"name"]];
    [self.navigationController pushViewController:dramaVC animated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end

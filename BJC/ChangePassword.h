//
//  ChangePassword.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassword : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *passwordOldTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewReTypeTextField;

- (IBAction)backClicked:(id)sender;
- (IBAction)homeClicked:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;

@end

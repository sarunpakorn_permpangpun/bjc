//
//  AppDelegate.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 7/23/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

#define widthScreen [UIScreen mainScreen].bounds.size.width
#define heightScreen [UIScreen mainScreen].bounds.size.height

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define color_green_light UIColorFromRGB(0x00B26B)
#define color_green UIColorFromRGB(0x188040)
#define color_gray UIColorFromRGB(0xD7D7D7)

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) id tracker;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *dicProfile;
@property (strong, nonatomic) NSString *pageid;

@end


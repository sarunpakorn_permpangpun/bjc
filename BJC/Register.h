//
//  Register.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XDPopupListView.h"

#import "Login.h"

@interface Register : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, XDPopupListViewDataSource, XDPopupListViewDelegate>
{
    XDPopupListView *mTextDropDownListView;
}


@property (weak, nonatomic) IBOutlet UIView *viewRegister;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *roadTextField;
@property (weak, nonatomic) IBOutlet UITextField *subdistrictTextField;
@property (weak, nonatomic) IBOutlet UITextField *districtTextField;
@property (weak, nonatomic) IBOutlet UITextField *proviceTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipcodeTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *telTextField;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollInputData;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *viewInputData;

- (IBAction)saveClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)genderClicked:(id)sender;

- (IBAction)subdistrictClicked:(id)sender;
- (IBAction)districtClicked:(id)sender;
- (IBAction)proviceClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)returnPicker:(id)sender;
- (IBAction)cancelPicker:(id)sender;


@end

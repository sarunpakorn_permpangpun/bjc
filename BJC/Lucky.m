//
//  Lucky.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Lucky.h"
#import "AppDelegate.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"
#import "NewsDescription.h"
#import "UIImageView+WebCache.h"

#import <Google/Analytics.h>
#import "LuckyCampaign.h"
#import "LuckyCoupon.h"

@interface Lucky ()
{
    NSMutableArray *listLucky;
    int count;
    
    MBProgressHUD *hudView;
    BOOL isLoader;
    
    NSUserDefaults *userDefaults;
}
@end

@implementation Lucky

- (void)viewDidLoad {

    [super viewDidLoad];
    
    count = 0;
    
    userDefaults = [[NSUserDefaults alloc]init];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    isLoader = YES;
    [self reqLucky:@"" :@"16" :^{
        isLoader = NO;
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ชิงโชค"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listLucky count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    NSDictionary *dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [dicLucky objectForKey:@"title"]];
    
    BOOL can_use = [[dicLucky objectForKey:@"can_use"] boolValue];
    
    if(!can_use) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicLucky objectForKey:@"message"]];
        cell.detailTextLabel.textColor = color_green;
    }
//    else {
//        cell.detailTextLabel.text = @"ไม่สามารถใช้งานได้";
//        cell.detailTextLabel.textColor = [UIColor redColor];
//    }

    
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [dicLucky objectForKey:@"thumbnail"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if(![thumbnail isEqualToString:@""])
    {
        [cell.imageView  sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:@"bjc_default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            UIImage *myIcon = [self imageByCroppingImage:image toSize:CGSizeMake(100, 100)];
            [cell.imageView setImage:myIcon];
        }];
    }else {
        UIImage *myIcon = [self imageByCroppingImage:[UIImage imageNamed:@"bjc_default.jpg"] toSize:CGSizeMake(100, 100)];
        [cell.imageView setImage:myIcon];
    }
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
    NSLog(@"%@", dicLucky);
    
    LuckyCampaign *luckyCampaignVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCampaign"];
    LuckyCoupon *luckyCouponVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LuckyCoupon"];

    if([[NSString stringWithFormat:@"%@",[dicLucky objectForKey:@"type"]]isEqualToString:@"campaign"]){
        luckyCampaignVC.dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:luckyCampaignVC animated:YES];
    }else if([[NSString stringWithFormat:@"%@",[dicLucky objectForKey:@"type"]]isEqualToString:@"coupon"]){
        luckyCouponVC.dicLucky = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listLucky objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:luckyCouponVC animated:YES];
        
    }
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)reqLucky: (NSString *)keyword :(NSString *)per_page :(void (^)(void))completion {
    
    [hudView show:YES];
    count++;
    RequestModel *service = [[RequestModel alloc]initGetLuckyDrawWithUsername:[NSString stringWithFormat:@"%@",  [userDefaults objectForKey:@"username"]] PerPage:per_page Page:[NSString stringWithFormat:@"%d", count] Type:@"campaign" Brand:@""];

    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    
                    listLucky = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                    
                    if(listLucky.count == 0) {
                        
                        [hudView show:YES];
                        
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                        
                        RequestModel *serviceGetMemberByEmail = [[RequestModel alloc]initGetMemberByEmailWithUsername:[NSString stringWithFormat:@"%@", [appDelegate.dicProfile objectForKey:@"user"]]];
                        
                        [serviceGetMemberByEmail requestDataWitnIndecator:^{
                            
                            [hudView hide:YES];
                            
                            if(serviceGetMemberByEmail.status)
                            {
                                NSDictionary *objectResult = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)serviceGetMemberByEmail.jsonData];
                                
                                BOOL isData = [[objectResult objectForKey:@"data"] boolValue];
                                
                                if(isData) {
                                    
                                    self.warnLabel.text = @"ระบบได้ทำการจับรางวัลเรียบร้อยแล้ว  โปรดรอลุ้นรางวัลในรอบถัดไป";
                                    
                                }else {
                                    
                                    self.warnLabel.text = @"ต้องการเข้าร่วมกิจกรรมชิงโชค กรุณากรอกข้อมูลลงทะเบียนของท่านให้ครบถ้วน";
                                    
                                }
                                
                                
                                self.warnLabel.hidden = NO;
                                
                                self.tableLucky.hidden = YES;
                                
                            }else{
                                
                                self.tableLucky.hidden = YES;
                                
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:serviceGetMemberByEmail.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [alert show];
                            }
                        }];
                    }
                    
                }else{
                    
                    @try{
                        
                        NSArray *listLuckyNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        
                        if(listLuckyNew.count>0){
                            
                            [listLucky addObjectsFromArray:listLuckyNew];
                            
                        }else{
                            count--;
                        }
                        
                        self.tableLucky.hidden = NO;
                        self.warnLabel.hidden = YES;
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableLucky reloadData];
                completion();
            }
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            [self reqLucky:@"" :@"16" :^{
                isLoader = NO;
            }];
        }
    }
}

@end

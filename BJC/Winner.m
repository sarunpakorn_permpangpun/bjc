//
//  Winner.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 1/20/16.
//  Copyright © 2016 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Winner.h"

@interface Winner ()

@end

@implementation Winner

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.titleTextView setText:[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"title"]]];
    [self.titleTextView sizeToFit];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[[NSString stringWithFormat:@"%@", [self.dicLucky objectForKey:@"code"]] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:14.0]
                             range:NSMakeRange(0, attributedString.length)];

    
    [self.detailTextView setAttributedText:attributedString];
    [self.detailTextView setFrame:CGRectMake(self.detailTextView.frame.origin.x, self.titleTextView.frame.origin.y+self.titleTextView.frame.size.height+8, self.detailTextView.frame.size.width, self.detailTextView.frame.size.height)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backClicked:(id)sender {
    if(self.isFromViewNotification) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end

//
//  MarketRelation.m
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/7/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "MarketRelation.h"
#import "AppDelegate.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"

@interface MarketRelation ()
{
    
    NSMutableArray *listMarketBase, *listMarket;
    
    MBProgressHUD *hudView;
}
@end
@implementation MarketRelation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    [self reqMarket:^(BOOL isSuccess) {
       
        if(isSuccess) {
            
            listMarket = [[NSMutableArray alloc]initWithArray:(NSArray*)listMarketBase];
            
        }else {
            
            listMarket = [[NSMutableArray alloc]init];
            
        }
        
        [self.tableMarket reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listMarket count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *dicMarket = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listMarket objectAtIndex:indexPath.row]];
    
    UIView *viewCell = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 96)];
    int mod = indexPath.row%2;
    if(mod==0)
    {
        [viewCell setBackgroundColor:[UIColor whiteColor]];
    }else{
        [viewCell setBackgroundColor:color_gray];
    }
    
    UILabel *labelName = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, viewCell.frame.size.width-16, 21)];
    labelName.text = [NSString stringWithFormat:@"ชื่อร้านค้า %@", [dicMarket objectForKey:@"name"]];
    labelName.backgroundColor = [UIColor clearColor];
    labelName.font = [UIFont systemFontOfSize:17.0f];
    [labelName setAdjustsFontSizeToFitWidth:YES];
    
    UILabel *labelAddress = [[UILabel alloc]initWithFrame:CGRectMake(8, 37, viewCell.frame.size.width-16, 43)];
    labelAddress.text = [NSString stringWithFormat:@"ที่อยู่ %@ %@ %@ %@ %@", [dicMarket objectForKey:@"address"], [dicMarket objectForKey:@"subdistrict"], [dicMarket objectForKey:@"district"], [dicMarket objectForKey:@"province"], [dicMarket objectForKey:@"zipcode"]];
    labelAddress.backgroundColor = [UIColor clearColor];
    labelAddress.textColor = [UIColor grayColor];
    labelAddress.font = [UIFont systemFontOfSize:14.0f];
    labelAddress.numberOfLines = 0;
    [labelAddress setAdjustsFontSizeToFitWidth:YES];
    
    [viewCell addSubview:labelName];
    [viewCell addSubview:labelAddress];
    [cell.contentView addSubview:viewCell];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)reqMarket :(void (^)(BOOL isSuccess))completion {
    
    [hudView show:YES];
    
    RequestModel *service = [[RequestModel alloc]initGetCouponStore:self.coupon_id PerPage:@"-1" Page:@"1"];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            if(completion)
            {
                listMarketBase = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
               
                completion(YES);
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            completion(NO);
        }
        
    }];
}

- (IBAction)backClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okClicked:(id)sender {
    
    [self searchReload];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [self.textSearch setShowsCancelButton:NO];
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self searchReload];
}


- (void)searchReload {
    
    NSString *searchTextString = self.textSearch.text;
    
    if(searchTextString.length > 0) {
        
        [listMarket removeAllObjects];
        
        for(NSDictionary *dicMarket in listMarketBase) {
            
            NSString *string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@", [dicMarket objectForKey:@"name"], [dicMarket objectForKey:@"address"], [dicMarket objectForKey:@"subdistrict"], [dicMarket objectForKey:@"district"], [dicMarket objectForKey:@"province"], [dicMarket objectForKey:@"zipcode"]];
            
            NSRange stringRange = [string rangeOfString:searchTextString];
            
            if (stringRange.location != NSNotFound) {
                
                [listMarket addObject:dicMarket];
            }
        }
    }else {
        
        listMarket = [[NSMutableArray alloc]initWithArray:(NSArray*)listMarketBase];
    }
    
    [self.tableMarket reloadData];
    
    [self.textSearch setShowsCancelButton:NO animated:YES];
    
    [self.view endEditing:YES];
    
    [self.tableMarket setContentOffset:CGPointMake(0, 0) animated:YES];
}

@end

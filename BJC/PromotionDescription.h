//
//  PromotionDescription.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/3/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface PromotionDescription : UIViewController <FBSDKSharingDelegate,  UIWebViewDelegate>

@property (strong, nonatomic) NSDictionary *dicPromotion;

@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIWebView *contentWebView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewLine;

- (IBAction)backClicked:(id)sender;
- (IBAction)facebookClicked:(id)sender;

@end

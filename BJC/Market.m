//
//  Market.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Market.h"
#import "AppDelegate.h"
#import "RequestModel.h"
#import "MBProgressHUD.h"

@interface Market ()
{
    NSMutableArray *listMarket;
    NSString *nameMarket;
    int count;
    
    MBProgressHUD *hudView;
    BOOL isLoader;
}
@end

@implementation Market

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 0;
    nameMarket = @"";
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    isLoader = YES;
    [self reqMarket:nameMarket :@"16" :^{
        isLoader = NO;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listMarket count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *dicMarket = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listMarket objectAtIndex:indexPath.row]];
    
    UIView *viewCell = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 96)];
    int mod = indexPath.row%2;
    if(mod==0)
    {
        [viewCell setBackgroundColor:[UIColor whiteColor]];
    }else{
        [viewCell setBackgroundColor:color_gray];
    }
    
    UILabel *labelName = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, viewCell.frame.size.width-16, 21)];
    labelName.text = [NSString stringWithFormat:@"ชื่อร้านค้า %@", [dicMarket objectForKey:@"name"]];
    labelName.backgroundColor = [UIColor clearColor];
    labelName.font = [UIFont systemFontOfSize:17.0f];
    [labelName setAdjustsFontSizeToFitWidth:YES];
    
    UILabel *labelAddress = [[UILabel alloc]initWithFrame:CGRectMake(8, 37, viewCell.frame.size.width-16, 43)];
    labelAddress.text = [NSString stringWithFormat:@"ที่อยู่ %@ %@ %@ %@ %@", [dicMarket objectForKey:@"address"], [dicMarket objectForKey:@"subdistrict"], [dicMarket objectForKey:@"district"], [dicMarket objectForKey:@"province"], [dicMarket objectForKey:@"zipcode"]];

    labelAddress.backgroundColor = [UIColor clearColor];
    labelAddress.textColor = [UIColor grayColor];
    labelAddress.font = [UIFont systemFontOfSize:14.0f];
    labelAddress.numberOfLines = 0;
    [labelAddress setAdjustsFontSizeToFitWidth:YES];
    
    [viewCell addSubview:labelName];
    [viewCell addSubview:labelAddress];
    [cell.contentView addSubview:viewCell];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)reqMarket: (NSString *)keyword :(NSString *)per_page :(void (^)(void))completion {
    
    [hudView show:YES];
    count++;
    RequestModel *service = [[RequestModel alloc]initGetStoresWithKeyword:keyword PerPage:per_page Page:[NSString stringWithFormat:@"%d", count]];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    listMarket = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                    if([listMarket count]==0) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"ไม่พบร้านค้า" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                }else{
                    @try{
                        NSArray *listPromotionNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        if(listPromotionNew.count>0){
                            [listMarket addObjectsFromArray:listPromotionNew];
                        }else{
                            count--;
                        }
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableMarket reloadData];
                completion();
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)searchClicked:(id)sender {
    
    [self.searchBar resignFirstResponder];
    
    count = 0;
    nameMarket = self.searchBar.text;
    isLoader = YES;
    [self reqMarket:nameMarket :@"16" :^{
        isLoader = NO;
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        
        if(!isLoader)
        {
            isLoader = YES;
            [self reqMarket:nameMarket :@"16" :^{
                isLoader = NO;
            }];
        }
    }
}

@end
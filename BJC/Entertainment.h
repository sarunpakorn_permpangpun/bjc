//
//  Entertainment.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Entertainment : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *newsButton;
@property (weak, nonatomic) IBOutlet UIButton *dramaButton;
@property (weak, nonatomic) IBOutlet UITableView *tableEntertainment;
@property (weak, nonatomic) IBOutlet UIView *viewDrama;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewChannel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollDrama;
@property (weak, nonatomic) IBOutlet UIPageControl *pagingControl;

- (IBAction)backClicked:(id)sender;
- (IBAction)newsClicked:(id)sender;
- (IBAction)dramaClicked:(id)sender;
- (IBAction)tvButton:(id)sender;

@end

//
//  Login.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/29/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Login.h"
#import "MBProgressHUD.h"
#import "RequestModel.h"
#import "ForgetPassword.h"
#import "Register.h"

#import "AppDelegate.h"

#import "Menu.h"

@interface Login ()
{
    NSUserDefaults *userDefaults;
    AppDelegate *appDelegate;
}
@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        userDefaults = [NSUserDefaults standardUserDefaults];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)loginSkipClicked:(id)sender {
    [userDefaults setObject:[_usernameTextfield text] forKey:@"username"];
    [userDefaults setObject:[_passwordTextfield text] forKey:@"password"];
    [userDefaults synchronize];
    
    Menu *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    UINavigationController *menuNC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    menuNC.navigationBarHidden = YES;
    [self presentViewController:menuNC animated:YES completion:nil];
}

- (IBAction)loginButton:(id)sender {
    [userDefaults setObject:[_usernameTextfield text] forKey:@"username"];
    [userDefaults setObject:[_passwordTextfield text] forKey:@"password"];
    [userDefaults setObject:@"1" forKey:@"showHowtoMenu"];
    [userDefaults setObject:@"1" forKey:@"showHowtoPromotion"];
    [userDefaults synchronize];
    [self reqLogin];
}

- (void) viewWillAppear:(BOOL)animated {
    
    
    NSObject *objectUsername = [userDefaults objectForKey:@"username"];
    NSObject *objectPassword = [userDefaults objectForKey:@"password"];
    if(objectUsername&&objectPassword) {
        
        if(![[NSString stringWithFormat:@"%@", objectUsername] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@", objectPassword] isEqualToString:@""]) {
            
            [self.viewPattan setHidden:YES];
            [self reqLogin];
            
        }else {
            
            [self.viewPattan setHidden:NO];
        }
    }else{
        [self.viewPattan setHidden:NO];
    }
}

- (IBAction)fbLoginButton:(id)sender {
    //    @try {
    //
    //        [FBSession.activeSession openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
    //            //        if(!FBSession.activeSession.isOpen)
    //            //        {
    //            //
    //            //        }
    //            //        else
    //            //        {
    //            [[FBRequest requestForMe] startWithCompletionHandler: ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
    //                if (!error)
    //                {
    //                    [self reqRegister:[NSString stringWithFormat:@"%@", user.objectID] :[NSString stringWithFormat:@"%@", user.objectID]];
    //                }
    //                else
    //                {
    //                    NSLog(@"%@", error);
    //                }
    //            }];
    //            //        }
    //        }];
    //
    //    }
    //    @catch (NSException *exception) {
    //
    //    }
}

- (IBAction)registerButton:(id)sender {
    
    [userDefaults removeObjectForKey:@"username"];
    [userDefaults removeObjectForKey:@"password"];
    
    if([userDefaults synchronize]) {
        
        Register *registerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Register"];
        [self.navigationController pushViewController:registerVC animated:YES];
    }
}


- (IBAction)forgetButton:(id)sender {
    ForgetPassword *forgetPasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPassword"];
    [self.navigationController pushViewController:forgetPasswordVC animated:YES];
}

-(void) reqLogin{
    MBProgressHUD *hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    [hudView show:YES];
    RequestModel *service = [[RequestModel alloc]initLoginWithUsername:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"username"]]Password:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"password"]] RegisterId:[NSString stringWithFormat:@"%@", [userDefaults objectForKey:@"token"]] Device:@"ios"];
    [service requestDataWitnIndecator:^{
        [hudView hide:YES];
        if(service.status)
        {
            [self.usernameTextfield setText:@""];
            [self.passwordTextfield setText:@""];
            
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            appDelegate.dicProfile = [[NSMutableDictionary alloc]initWithDictionary:(NSDictionary *)[service.jsonData objectForKey:@"data"]];
            Menu *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
            UINavigationController *menuNC = [[UINavigationController alloc] initWithRootViewController:menuVC];
            menuNC.navigationBarHidden = YES;
            [self presentViewController:menuNC animated:YES completion:nil];
        }
        else
        {
            
            [userDefaults setObject:@"0" forKey:@"showHowtoMenu"];
            [userDefaults setObject:@"0" forKey:@"showHowtoPromotion"];
            [userDefaults synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end

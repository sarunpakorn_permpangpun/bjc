//
//  ForgetPassword.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPassword : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (strong, nonatomic) IBOutlet UIView *viewMessage;

- (IBAction)submitButton:(id)sender;
- (IBAction)clearButton:(id)sender;
- (IBAction)backButton:(id)sender;

@end

//
//  InformationUpdate.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationUpdate : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *goldButton;
@property (weak, nonatomic) IBOutlet UIButton *oilButton;
@property (weak, nonatomic) IBOutlet UIButton *lottoButton;

@property (weak, nonatomic) IBOutlet UIView *viewGold;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceBuyGoldBarLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceSaleGoldBarLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceBuyGoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceSaleGoldLabel;

@property (weak, nonatomic) IBOutlet UIView *viewOil;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *oline91Label;
@property (weak, nonatomic) IBOutlet UILabel *hol91Label;
@property (weak, nonatomic) IBOutlet UILabel *hol95Label;
@property (weak, nonatomic) IBOutlet UILabel *holE20Label;
@property (weak, nonatomic) IBOutlet UILabel *holE85Label;
@property (weak, nonatomic) IBOutlet UILabel *diesalLabel;
@property (weak, nonatomic) IBOutlet UILabel *ngvLabel;

@property (weak, nonatomic) IBOutlet UIView *viewLotto;
@property (weak, nonatomic) IBOutlet UILabel *dateLottoLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLottoFirst;
@property (weak, nonatomic) IBOutlet UILabel *numberLottoSecond;
@property (weak, nonatomic) IBOutlet UILabel *numberLottoThird;
@property (weak, nonatomic) IBOutlet UILabel *numberLottoThirdBack;

- (IBAction)changeClicked:(id)sender;
- (IBAction)backClicked:(id)sender;

@end

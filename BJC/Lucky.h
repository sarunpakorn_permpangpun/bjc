//
//  Lucky.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lucky : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableLucky;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellLucky;
@property (weak, nonatomic) IBOutlet UILabel *warnLabel;

- (IBAction)backClicked:(id)sender;

@end

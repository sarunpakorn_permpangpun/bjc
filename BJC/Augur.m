//
//  Augur.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Augur.h"
#import "RequestModelADV.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import <Google/Analytics.h>

@interface Augur ()
{
    NSArray *listBirthDay;
    NSArray *listImageHoro;
    NSMutableArray *listHoro;
    
    MBProgressHUD *hudView;
}
@end

@implementation Augur

- (void)viewDidLoad {
    [super viewDidLoad];
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    listBirthDay = [[NSArray alloc]initWithObjects:
                    @"อาทิตย์",
                    @"จันทร์",
                    @"อังคาร",
                    @"พุธ",
                    @"พฤหัสบดี",
                    @"ศุกร์",
                    @"เสาร์",
                    nil];
    listImageHoro = [[NSArray alloc]initWithObjects:
                     @"sun.png",
                     @"mon.png",
                     @"tue.png",
                     @"wed.png",
                     @"thu.png",
                     @"fri.png",
                     @"sat.png",
                     nil];
    NSArray *listMonth = [[NSArray alloc]initWithObjects:
                          @"ม.ค.",
                          @"ก.พ.",
                          @"มี.ค.",
                          @"เม.ย.",
                          @"พ.ค.",
                          @"มิ.ย.",
                          @"ก.ค.",
                          @"ส.ค.",
                          @"ก.ย.",
                          @"ต.ค.",
                          @"พ.ย.",
                          @"ธ.ค.",
                     nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger day = [components day];
    NSInteger week = [components month];
    NSInteger year = [components year];
    
    NSString *dateString = @"";
    NSString *monthString = @"";
    
//    if(day<10)
//    {
//        dateString = [NSString stringWithFormat:@"0%ld", (long)day];
//    }else{
        dateString = [NSString stringWithFormat:@"%ld", (long)day];
//    }
    
    monthString = [NSString stringWithFormat:@"%@", [listMonth objectAtIndex:week-1]];
    
    if(year<2500)
    {
        year+=543;
    }
    
    self.dateNowLabel.text = [NSString stringWithFormat:@"ประจำวันที่ %@ %@ %ld", dateString, monthString, (long)year];
    [self reqHoroscope:@"20141201" :^{
        [self.tableAugur reloadData];
        [self.tableAugur setHidden:NO];
        [hudView hide:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listBirthDay count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    float width = [UIScreen mainScreen].bounds.size.width-100;
    UILabel *labelDetail = [[UILabel alloc]initWithFrame:CGRectMake(80, 37, width, 26)];
    [labelDetail setText:[NSString stringWithFormat:@"%@", [listHoro objectAtIndex:indexPath.row]]];
    [labelDetail setBackgroundColor:[UIColor clearColor]];
    [labelDetail setFont:[UIFont systemFontOfSize:13.0f]];
    [labelDetail setNumberOfLines:0];
    [labelDetail sizeToFit];
    
    if((indexPath.row%2)!=0){
        [cell.contentView setBackgroundColor:color_gray];
    }
    
    UIImageView *imageHoro = [[UIImageView alloc]initWithFrame:CGRectMake(6, 8, 60, 60)];
    [imageHoro setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [listImageHoro objectAtIndex:indexPath.row]]]];
    
       UILabel *labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(80, 8, 200, 21)];
        [labelTitle setText:[NSString stringWithFormat:@"คนที่เกิดวัน%@", [listBirthDay objectAtIndex:indexPath.row]]];
        [labelTitle setBackgroundColor:[UIColor clearColor]];
        [labelTitle setFont:[UIFont systemFontOfSize:18.0f]];
        [labelTitle setTextColor:color_green];
    
    [cell.contentView addSubview:labelTitle];
    [cell.contentView addSubview:labelDetail];
    [cell.contentView addSubview:imageHoro];
    
    float isHeightValue = labelDetail.frame.origin.y + labelDetail.frame.size.height + 8;
    [self.tableAugur setRowHeight:isHeightValue];
    
    return cell;
}

- (void)reqHoroscope: (NSString *)date :(void (^)(void))completion {

    [hudView show:YES];
    RequestModelADV *service = [[RequestModelADV alloc]initGetHoroscopeWithDate:date];
    [service requestDataWitnIndecator:^{
        if(service.status)
        {
            NSDictionary *dicData = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[service.jsonData objectForKey:@"data"]];
            listHoro = [[NSMutableArray alloc]init];
            [listHoro addObject:[dicData objectForKey:@"sun"]];
            [listHoro addObject:[dicData objectForKey:@"mon"]];
            [listHoro addObject:[dicData objectForKey:@"tue"]];
            [listHoro addObject:[dicData objectForKey:@"wed"]];
            [listHoro addObject:[dicData objectForKey:@"thu"]];
            [listHoro addObject:[dicData objectForKey:@"fri"]];
            [listHoro addObject:[dicData objectForKey:@"sat"]];
            
            if(completion)
            {
                completion();
            }
        }
        else
        {
            [hudView hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}


- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ดูดวง"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end
//
//  DramaDescription.h
//  BJC
//
//  Created by Sarunpakorn Permpangpun on 9/4/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DramaDescription : UIViewController

@property (strong, nonatomic) NSDictionary *dicDrama;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *chapterLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

- (IBAction)backClicked:(id)sender;

@end

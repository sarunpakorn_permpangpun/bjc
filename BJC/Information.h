//
//  Information.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Information : UIViewController

- (IBAction)backClicked:(id)sender;
- (IBAction)visitClicked:(id)sender;
- (IBAction)fbBJCClicked:(id)sender;

@end

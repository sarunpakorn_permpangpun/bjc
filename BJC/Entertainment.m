//
//  Entertainment.m
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import "Entertainment.h"
#import "Chanel.h"
#import "Drama.h"
#import "AppDelegate.h"
#import "RequestModelADV.h"
#import "MBProgressHUD.h"
#import "EntertainmentDescription.h"
#import "UIImageView+WebCache.h"

#import <Google/Analytics.h>

@interface Entertainment ()
{
    NSMutableArray *listEntertainment, *listChannel, *listSeries;
    NSInteger indexSeries;
    
    int count;
    
    NSString *chanelTVString;
    BOOL isLoader;
    
    MBProgressHUD *hudView;
}
@end

@implementation Entertainment

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.labelText = @"Loading";
    [self.view addSubview:hudView];
    
    [self reloadButtonIsDrama:NO];
    
    count = 0;
    
    
    [self reqChannel:^{
        float marginTop = [UIScreen mainScreen].bounds.size.width/12;
        marginTop = marginTop / 2;
        float width = [UIScreen mainScreen].bounds.size.width/4;
        float marginLeft = marginTop*2;
        NSInteger indexChannel = 0;
        
        for (NSDictionary *objectChannel in listChannel) {
            
            UIButton *channelButton = [[UIButton alloc]initWithFrame:CGRectMake(marginLeft, marginTop, width, width)];
            [channelButton addTarget:self action:@selector(tvButton:) forControlEvents:UIControlEventTouchUpInside];
            channelButton.tag = indexChannel;
            NSString *thumbnail = [NSString stringWithFormat:@"%@", [objectChannel objectForKey:@"thumbnail"]];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
                NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail] options:NSDataReadingUncached error:nil];
                dispatch_sync(dispatch_get_main_queue(), ^(void) {
                    
                    UIImage *myIcon = [UIImage imageWithData:dataImage];
                    [channelButton setImage:myIcon forState:UIControlStateNormal];
                });
            });
            [self.scrollViewChannel addSubview:channelButton];
            
                indexChannel++;
                marginLeft = (width+marginTop)+marginLeft;
        }
        [self.scrollViewChannel setContentSize:CGSizeMake(marginLeft, self.scrollViewChannel.frame.size.height)];

        indexSeries = 0;
        listSeries = [[NSMutableArray alloc]init];
        [self reqSeriesName :@"1" :@"1"];
    }];
}

- (void) reloadButtonIsDrama:(BOOL) isDrama {
    if(!isDrama){
        [self.newsButton setImage:[UIImage imageNamed:@"button8-1.png"] forState:UIControlStateNormal];
        [self.dramaButton setImage:[UIImage imageNamed:@"button9-2.png"] forState:UIControlStateNormal];
        [self.viewDrama setHidden:YES];
        [self.tableEntertainment setHidden:NO];
    }else{
        [self.newsButton setImage:[UIImage imageNamed:@"button8-2.png"] forState:UIControlStateNormal];
        [self.dramaButton setImage:[UIImage imageNamed:@"button9-1.png"] forState:UIControlStateNormal];
        [self.viewDrama setHidden:NO];
        [self.tableEntertainment setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)newsClicked:(id)sender {
    [self reloadButtonIsDrama:NO];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ข่าวบันเทิง"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)dramaClicked:(id)sender {
    [self reloadButtonIsDrama:YES];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"เรื่องย่อละคร"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listEntertainment count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    
    NSDictionary *dicNews = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listEntertainment objectAtIndex:indexPath.row]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"title"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"date"]];
    NSString *thumbnail = [NSString stringWithFormat:@"%@", [dicNews objectForKey:@"thumbnail"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
   
    if(![thumbnail isEqualToString:@""]) {
        
        [cell.imageView  sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:@"bjc_default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            UIImage *myIcon = [self imageByCroppingImage:image toSize:CGSizeMake(100, 100)];
            [cell.imageView setImage:myIcon];
        }];
    }else {
        UIImage *myIcon = [self imageByCroppingImage:[UIImage imageNamed:@"bjc_default.jpg"] toSize:CGSizeMake(100, 100)];
        [cell.imageView setImage:myIcon];
    }
    
    return cell;
}

- (UIImage *) scaleProportionalToSize:(UIImage *)image :(CGSize)size
{
    float widthRatio = size.width/image.size.width;
    float heightRatio = size.height/image.size.height;
    
    if(widthRatio > heightRatio)
    {
        size=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    } else {
        size=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    return [self imageWithImage:image scaledToSize:size];
}

- (UIImage *) imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    float widthNewSize = newSize.width;
    float heightNewSize = newSize.height;
    float widthOriginalSize = image.size.width;
    float heightOriginalSize = image.size.height;
    
    if(widthOriginalSize>0 && heightNewSize==0)
    {
        newSize.height = (heightOriginalSize/widthOriginalSize)*widthNewSize;
        
    }else if(widthNewSize==0 && heightNewSize>0)
    {
        newSize.width = (widthOriginalSize/heightOriginalSize)*heightNewSize;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EntertainmentDescription *entertainmentDescriptionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EntertainmentDescription"];
    entertainmentDescriptionVC.dicEntertainment = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listEntertainment objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:entertainmentDescriptionVC animated:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    if(aScrollView.tag==1)
    {
        CGFloat pageWidth = self.scrollDrama.frame.size.width;
        int page = floor((self.scrollDrama.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pagingControl.currentPage = page;
    }else{
        
        CGPoint offset = aScrollView.contentOffset;
        CGRect bounds = aScrollView.bounds;
        CGSize size = aScrollView.contentSize;
        UIEdgeInsets inset = aScrollView.contentInset;
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        float reload_distance = 10;
        if(y > h + reload_distance) {
            
            if(!isLoader)
            {
                isLoader = YES;
                [self reqEntertainment:@"" :@"16":^{
                    isLoader = NO;
                }];
            }
        }
    }
}

- (IBAction)tvButton:(id)sender {
    
    NSDictionary *objectChannel = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listChannel objectAtIndex:[sender tag]]];
    
    Chanel *chanelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Chanel"];
    chanelVC.tvChanel = [NSString stringWithFormat:@"%@", [objectChannel objectForKey:@"slug"]];
    
    [self.navigationController pushViewController:chanelVC animated:YES];
}

- (void)reqEntertainment: (NSString *)keyword :(NSString *)per_page :(void (^)(void))completion {

    [hudView show:YES];
    count++;
    RequestModelADV *service = [[RequestModelADV alloc]initGetEntertainmentsWithKeyword:keyword PerPage:per_page Page:[NSString stringWithFormat:@"%d", count]];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            if(completion)
            {
                if(count==1){
                    listEntertainment = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                }else{
                    @try{
                        NSArray *listEntertainmentNew = [[NSArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                        if(listEntertainmentNew.count>0){
                            [listEntertainment addObjectsFromArray:listEntertainmentNew];
                        }else{
                            count--;
                        }
                    }
                    @catch (NSException *exception) {
                        count--;
                    }
                }
                [self.tableEntertainment reloadData];
                completion();
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)reqChannel :(void (^)(void))completion {
    
    [hudView show:YES];
    RequestModelADV *service = [[RequestModelADV alloc]initGetChannel];
    [service requestDataWitnIndecator:^{[hudView hide:YES];
        if(service.status)
        {
            if(completion)
            {
                listChannel = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                completion();
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}


- (void)reqSeriesName:(NSString *)per_page :(NSString *)page{

    [hudView show:YES];
    
    if(indexSeries < listChannel.count)
    {
        NSDictionary *channelInfo = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listChannel objectAtIndex:indexSeries]];
        chanelTVString = [NSString stringWithFormat:@"%@", [channelInfo objectForKey:@"slug"]];
        RequestModelADV *service = [[RequestModelADV alloc]initGetSeriesNameWithChannel:chanelTVString PerPage:per_page Page:page];
        [service requestDataWitnIndecator:^{
            if(service.status)
            {
                NSMutableArray *resultListSeries = [[NSMutableArray alloc]initWithArray:(NSArray *)[service.jsonData objectForKey:@"rows"]];
                if(resultListSeries.count>0)
                {
                    [listSeries addObject:(NSDictionary*)[resultListSeries objectAtIndex:0]];
                }
                indexSeries++;
                [self reqSeriesName :@"1" :@"1"];
            }
            else
            {
                [hudView hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BJC" message:service.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }else{
        [self reloadSeriesName:^{
            [self reqEntertainment:@"" :@"16" :^{
                [hudView hide:YES];
            }];
        }];
    }
}

- (void) reloadSeriesName :(void (^)(void))completion {
    
    float x = 0;
    NSInteger tagID = 0;
    for (NSDictionary *objectSeries in listSeries) {
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[objectSeries objectForKey:@"thumbnail"]]];
        UIButton *buttonImage = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, [UIScreen mainScreen].bounds.size.width, self.scrollDrama.frame.size.height)];
        [buttonImage setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
        [buttonImage.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [buttonImage setTag:tagID];
        [buttonImage addTarget:self action:@selector(dramaChoiceClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollDrama addSubview:buttonImage];
        
        x+=[UIScreen mainScreen].bounds.size.width;
        tagID++;
    }
    
    if(completion)
    {
        [self.pagingControl setNumberOfPages:listSeries.count];
        [self.scrollDrama setContentSize:CGSizeMake(x, self.scrollDrama.frame.size.height)];
        completion();
    }
}

- (IBAction)dramaChoiceClicked:(id)sender {
//    UIButton *button = (UIButton*)sender;
    NSInteger idButton = [sender tag];
    Drama *dramaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Drama"];
    NSDictionary *dicDrama = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)[listSeries objectAtIndex:idButton]];
    dramaVC.slug = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"slug"]];
    dramaVC.nameDrama = [NSString stringWithFormat:@"%@", [dicDrama objectForKey:@"name"]];
    [self.navigationController pushViewController:dramaVC animated:YES];
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"บันเทิง"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end

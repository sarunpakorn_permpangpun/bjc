//
//  Menu.h
//  BCJ
//
//  Created by Sarunpakorn Permpangpun on 4/30/2558 BE.
//  Copyright (c) 2558 Sarunpakorn Permpangpun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageDisplayCartoon;
@property (weak, nonatomic) IBOutlet UIImageView *labelDisplay;
//@property (weak, nonatomic) IBOutlet UIImageView *newNoticeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *noticeImageView;
@property (weak, nonatomic) IBOutlet UIButton *buttonHowto;
- (IBAction)informationClicked:(id)sender;
- (IBAction)noticeClicked:(id)sender;
- (IBAction)settingClicked:(id)sender;
- (IBAction)menuClicked:(id)sender;
- (IBAction)howtoClicked:(id)sender;

@end
